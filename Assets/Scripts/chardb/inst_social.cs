using System.Collections.Generic;

public class social_friend : BinaryHelper.ISerializable
{
	public uint characterId;
	public uint friendId;
	public long createTime;

	public static string GetTableName() {
		return "social_friend";
	}
	public static string GetTableKeyName() {
		return string.Empty;
	}
	public uint GetTableKeyValue() {
		return (uint)0;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out characterId, buffer);
		BinaryHelper.ReadFromBuffer(out friendId, buffer);
		BinaryHelper.ReadFromBuffer(out createTime, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(characterId, buffer);
		BinaryHelper.WriteToBuffer(friendId, buffer);
		BinaryHelper.WriteToBuffer(createTime, buffer);
	}
};

public class social_ignore : BinaryHelper.ISerializable
{
	public uint characterId;
	public uint ignoreId;
	public long createTime;

	public static string GetTableName() {
		return "social_ignore";
	}
	public static string GetTableKeyName() {
		return string.Empty;
	}
	public uint GetTableKeyValue() {
		return (uint)0;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out characterId, buffer);
		BinaryHelper.ReadFromBuffer(out ignoreId, buffer);
		BinaryHelper.ReadFromBuffer(out createTime, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(characterId, buffer);
		BinaryHelper.WriteToBuffer(ignoreId, buffer);
		BinaryHelper.WriteToBuffer(createTime, buffer);
	}
};
