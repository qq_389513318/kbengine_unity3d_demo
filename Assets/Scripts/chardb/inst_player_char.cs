using System.Collections.Generic;

public class IpcFlags : BinaryHelper.ISerializable
{
	public bool isInit;

	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out isInit, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(isInit, buffer);
	}
};

public class IpcAppearance : BinaryHelper.ISerializable
{
	public uint charTypeId;
	public byte hairType;
	public byte hairColor;
	public byte face;
	public byte skin;

	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out charTypeId, buffer);
		BinaryHelper.ReadFromBuffer(out hairType, buffer);
		BinaryHelper.ReadFromBuffer(out hairColor, buffer);
		BinaryHelper.ReadFromBuffer(out face, buffer);
		BinaryHelper.ReadFromBuffer(out skin, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(charTypeId, buffer);
		BinaryHelper.WriteToBuffer(hairType, buffer);
		BinaryHelper.WriteToBuffer(hairColor, buffer);
		BinaryHelper.WriteToBuffer(face, buffer);
		BinaryHelper.WriteToBuffer(skin, buffer);
	}
};

public class IpcPropertyValue : BinaryHelper.ISerializable
{
	public uint bagCapacity;
	public uint bankCapacity;

	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out bagCapacity, buffer);
		BinaryHelper.ReadFromBuffer(out bankCapacity, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(bagCapacity, buffer);
		BinaryHelper.WriteToBuffer(bankCapacity, buffer);
	}
};

public class IpcRankValue : BinaryHelper.ISerializable
{
	public long lastLevelTime;
	public long lastFightValueTime;

	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out lastLevelTime, buffer);
		BinaryHelper.ReadFromBuffer(out lastFightValueTime, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(lastLevelTime, buffer);
		BinaryHelper.WriteToBuffer(lastFightValueTime, buffer);
	}
};

public class IpcGsReadValue : BinaryHelper.ISerializable
{
	public ulong lastFightValue;

	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out lastFightValue, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(lastFightValue, buffer);
	}
};

public class IpcGsWriteValue : BinaryHelper.ISerializable
{
	public long guildScore;

	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out guildScore, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(guildScore, buffer);
	}
};

public class InstPlayerPreviewInfo : BinaryHelper.ISerializable
{
	public uint ipcInstID;
	public string ipcNickName;
	public IpcAppearance ipcAppearance;
	public byte ipcCareer;
	public byte ipcGender;
	public uint ipcLevel;
	public long ipcMoneyGold;
	public long ipcMoneyDiamond;
	public string ipcEffectItems;

	public static string GetTableName() {
		return "inst_player_char";
	}
	public static string GetTableKeyName() {
		return "ipcInstID";
	}
	public uint GetTableKeyValue() {
		return (uint)ipcInstID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out ipcInstID, buffer);
		BinaryHelper.ReadFromBuffer(out ipcNickName, buffer);
		BinaryHelper.LoadFromBuffer(out ipcAppearance, buffer);
		BinaryHelper.ReadFromBuffer(out ipcCareer, buffer);
		BinaryHelper.ReadFromBuffer(out ipcGender, buffer);
		BinaryHelper.ReadFromBuffer(out ipcLevel, buffer);
		BinaryHelper.ReadFromBuffer(out ipcMoneyGold, buffer);
		BinaryHelper.ReadFromBuffer(out ipcMoneyDiamond, buffer);
		BinaryHelper.ReadFromBuffer(out ipcEffectItems, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(ipcInstID, buffer);
		BinaryHelper.WriteToBuffer(ipcNickName, buffer);
		BinaryHelper.SaveToBuffer(ipcAppearance, buffer);
		BinaryHelper.WriteToBuffer(ipcCareer, buffer);
		BinaryHelper.WriteToBuffer(ipcGender, buffer);
		BinaryHelper.WriteToBuffer(ipcLevel, buffer);
		BinaryHelper.WriteToBuffer(ipcMoneyGold, buffer);
		BinaryHelper.WriteToBuffer(ipcMoneyDiamond, buffer);
		BinaryHelper.WriteToBuffer(ipcEffectItems, buffer);
	}
};

public class InstPlayerOutlineInfo : BinaryHelper.ISerializable
{
	public uint ipcInstID;
	public uint ipcAcctID;
	public string ipcNickName;
	public IpcFlags ipcFlags;
	public byte ipcCareer;
	public byte ipcGender;
	public ushort ipcMapType;
	public ushort ipcMapID;
	public float ipcPosX;
	public float ipcPosY;
	public float ipcPosZ;
	public float ipcPosO;
	public uint ipcLevel;
	public uint ipcServerID;
	public long ipcLastOnlineTime;
	public IpcRankValue ipcRankValue;
	public IpcGsReadValue ipcGsReadValue;
	public IpcGsWriteValue ipcGsWriteValue;

	public static string GetTableName() {
		return "inst_player_char";
	}
	public static string GetTableKeyName() {
		return "ipcInstID";
	}
	public uint GetTableKeyValue() {
		return (uint)ipcInstID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out ipcInstID, buffer);
		BinaryHelper.ReadFromBuffer(out ipcAcctID, buffer);
		BinaryHelper.ReadFromBuffer(out ipcNickName, buffer);
		BinaryHelper.LoadFromBuffer(out ipcFlags, buffer);
		BinaryHelper.ReadFromBuffer(out ipcCareer, buffer);
		BinaryHelper.ReadFromBuffer(out ipcGender, buffer);
		BinaryHelper.ReadFromBuffer(out ipcMapType, buffer);
		BinaryHelper.ReadFromBuffer(out ipcMapID, buffer);
		BinaryHelper.ReadFromBuffer(out ipcPosX, buffer);
		BinaryHelper.ReadFromBuffer(out ipcPosY, buffer);
		BinaryHelper.ReadFromBuffer(out ipcPosZ, buffer);
		BinaryHelper.ReadFromBuffer(out ipcPosO, buffer);
		BinaryHelper.ReadFromBuffer(out ipcLevel, buffer);
		BinaryHelper.ReadFromBuffer(out ipcServerID, buffer);
		BinaryHelper.ReadFromBuffer(out ipcLastOnlineTime, buffer);
		BinaryHelper.LoadFromBuffer(out ipcRankValue, buffer);
		BinaryHelper.LoadFromBuffer(out ipcGsReadValue, buffer);
		BinaryHelper.LoadFromBuffer(out ipcGsWriteValue, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(ipcInstID, buffer);
		BinaryHelper.WriteToBuffer(ipcAcctID, buffer);
		BinaryHelper.WriteToBuffer(ipcNickName, buffer);
		BinaryHelper.SaveToBuffer(ipcFlags, buffer);
		BinaryHelper.WriteToBuffer(ipcCareer, buffer);
		BinaryHelper.WriteToBuffer(ipcGender, buffer);
		BinaryHelper.WriteToBuffer(ipcMapType, buffer);
		BinaryHelper.WriteToBuffer(ipcMapID, buffer);
		BinaryHelper.WriteToBuffer(ipcPosX, buffer);
		BinaryHelper.WriteToBuffer(ipcPosY, buffer);
		BinaryHelper.WriteToBuffer(ipcPosZ, buffer);
		BinaryHelper.WriteToBuffer(ipcPosO, buffer);
		BinaryHelper.WriteToBuffer(ipcLevel, buffer);
		BinaryHelper.WriteToBuffer(ipcServerID, buffer);
		BinaryHelper.WriteToBuffer(ipcLastOnlineTime, buffer);
		BinaryHelper.SaveToBuffer(ipcRankValue, buffer);
		BinaryHelper.SaveToBuffer(ipcGsReadValue, buffer);
		BinaryHelper.SaveToBuffer(ipcGsWriteValue, buffer);
	}
};

public class inst_player_char : BinaryHelper.ISerializable
{
	public uint ipcInstID;
	public uint ipcAcctID;
	public string ipcNickName;
	public IpcFlags ipcFlags;
	public IpcAppearance ipcAppearance;
	public byte ipcCareer;
	public byte ipcGender;
	public ushort ipcMapType;
	public ushort ipcMapID;
	public float ipcPosX;
	public float ipcPosY;
	public float ipcPosZ;
	public float ipcPosO;
	public uint ipcLevel;
	public long ipcExp;
	public long ipcCurHP;
	public long ipcCurMP;
	public long ipcMoneyGold;
	public long ipcMoneyDiamond;
	public uint ipcServerID;
	public long ipcCreateTime;
	public long ipcLastLoginTime;
	public long ipcLastOnlineTime;
	public long ipcDeleteTime;
	public string ipcQuestsDone;
	public string ipcQuests;
	public string ipcEffectItems;
	public string ipcOtherItems;
	public string ipcItemStorage;
	public string ipcShopStatus;
	public string ipcSpells;
	public string ipcBuffs;
	public string ipcCooldowns;
	public string ipcJsonValue;
	public List<float> ipcF32Values;
	public List<int> ipcS32Values;
	public List<long> ipcS64Values;
	public IpcPropertyValue ipcPropertyValue;
	public IpcRankValue ipcRankValue;
	public IpcGsReadValue ipcGsReadValue;
	public IpcGsWriteValue ipcGsWriteValue;

	public static string GetTableName() {
		return "inst_player_char";
	}
	public static string GetTableKeyName() {
		return "ipcInstID";
	}
	public uint GetTableKeyValue() {
		return (uint)ipcInstID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out ipcInstID, buffer);
		BinaryHelper.ReadFromBuffer(out ipcAcctID, buffer);
		BinaryHelper.ReadFromBuffer(out ipcNickName, buffer);
		BinaryHelper.LoadFromBuffer(out ipcFlags, buffer);
		BinaryHelper.LoadFromBuffer(out ipcAppearance, buffer);
		BinaryHelper.ReadFromBuffer(out ipcCareer, buffer);
		BinaryHelper.ReadFromBuffer(out ipcGender, buffer);
		BinaryHelper.ReadFromBuffer(out ipcMapType, buffer);
		BinaryHelper.ReadFromBuffer(out ipcMapID, buffer);
		BinaryHelper.ReadFromBuffer(out ipcPosX, buffer);
		BinaryHelper.ReadFromBuffer(out ipcPosY, buffer);
		BinaryHelper.ReadFromBuffer(out ipcPosZ, buffer);
		BinaryHelper.ReadFromBuffer(out ipcPosO, buffer);
		BinaryHelper.ReadFromBuffer(out ipcLevel, buffer);
		BinaryHelper.ReadFromBuffer(out ipcExp, buffer);
		BinaryHelper.ReadFromBuffer(out ipcCurHP, buffer);
		BinaryHelper.ReadFromBuffer(out ipcCurMP, buffer);
		BinaryHelper.ReadFromBuffer(out ipcMoneyGold, buffer);
		BinaryHelper.ReadFromBuffer(out ipcMoneyDiamond, buffer);
		BinaryHelper.ReadFromBuffer(out ipcServerID, buffer);
		BinaryHelper.ReadFromBuffer(out ipcCreateTime, buffer);
		BinaryHelper.ReadFromBuffer(out ipcLastLoginTime, buffer);
		BinaryHelper.ReadFromBuffer(out ipcLastOnlineTime, buffer);
		BinaryHelper.ReadFromBuffer(out ipcDeleteTime, buffer);
		BinaryHelper.ReadFromBuffer(out ipcQuestsDone, buffer);
		BinaryHelper.ReadFromBuffer(out ipcQuests, buffer);
		BinaryHelper.ReadFromBuffer(out ipcEffectItems, buffer);
		BinaryHelper.ReadFromBuffer(out ipcOtherItems, buffer);
		BinaryHelper.ReadFromBuffer(out ipcItemStorage, buffer);
		BinaryHelper.ReadFromBuffer(out ipcShopStatus, buffer);
		BinaryHelper.ReadFromBuffer(out ipcSpells, buffer);
		BinaryHelper.ReadFromBuffer(out ipcBuffs, buffer);
		BinaryHelper.ReadFromBuffer(out ipcCooldowns, buffer);
		BinaryHelper.ReadFromBuffer(out ipcJsonValue, buffer);
		BinaryHelper.SequenceReadFromBuffer(out ipcF32Values, buffer);
		BinaryHelper.SequenceReadFromBuffer(out ipcS32Values, buffer);
		BinaryHelper.SequenceReadFromBuffer(out ipcS64Values, buffer);
		BinaryHelper.LoadFromBuffer(out ipcPropertyValue, buffer);
		BinaryHelper.LoadFromBuffer(out ipcRankValue, buffer);
		BinaryHelper.LoadFromBuffer(out ipcGsReadValue, buffer);
		BinaryHelper.LoadFromBuffer(out ipcGsWriteValue, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(ipcInstID, buffer);
		BinaryHelper.WriteToBuffer(ipcAcctID, buffer);
		BinaryHelper.WriteToBuffer(ipcNickName, buffer);
		BinaryHelper.SaveToBuffer(ipcFlags, buffer);
		BinaryHelper.SaveToBuffer(ipcAppearance, buffer);
		BinaryHelper.WriteToBuffer(ipcCareer, buffer);
		BinaryHelper.WriteToBuffer(ipcGender, buffer);
		BinaryHelper.WriteToBuffer(ipcMapType, buffer);
		BinaryHelper.WriteToBuffer(ipcMapID, buffer);
		BinaryHelper.WriteToBuffer(ipcPosX, buffer);
		BinaryHelper.WriteToBuffer(ipcPosY, buffer);
		BinaryHelper.WriteToBuffer(ipcPosZ, buffer);
		BinaryHelper.WriteToBuffer(ipcPosO, buffer);
		BinaryHelper.WriteToBuffer(ipcLevel, buffer);
		BinaryHelper.WriteToBuffer(ipcExp, buffer);
		BinaryHelper.WriteToBuffer(ipcCurHP, buffer);
		BinaryHelper.WriteToBuffer(ipcCurMP, buffer);
		BinaryHelper.WriteToBuffer(ipcMoneyGold, buffer);
		BinaryHelper.WriteToBuffer(ipcMoneyDiamond, buffer);
		BinaryHelper.WriteToBuffer(ipcServerID, buffer);
		BinaryHelper.WriteToBuffer(ipcCreateTime, buffer);
		BinaryHelper.WriteToBuffer(ipcLastLoginTime, buffer);
		BinaryHelper.WriteToBuffer(ipcLastOnlineTime, buffer);
		BinaryHelper.WriteToBuffer(ipcDeleteTime, buffer);
		BinaryHelper.WriteToBuffer(ipcQuestsDone, buffer);
		BinaryHelper.WriteToBuffer(ipcQuests, buffer);
		BinaryHelper.WriteToBuffer(ipcEffectItems, buffer);
		BinaryHelper.WriteToBuffer(ipcOtherItems, buffer);
		BinaryHelper.WriteToBuffer(ipcItemStorage, buffer);
		BinaryHelper.WriteToBuffer(ipcShopStatus, buffer);
		BinaryHelper.WriteToBuffer(ipcSpells, buffer);
		BinaryHelper.WriteToBuffer(ipcBuffs, buffer);
		BinaryHelper.WriteToBuffer(ipcCooldowns, buffer);
		BinaryHelper.WriteToBuffer(ipcJsonValue, buffer);
		BinaryHelper.SequenceWriteToBuffer(ipcF32Values, buffer);
		BinaryHelper.SequenceWriteToBuffer(ipcS32Values, buffer);
		BinaryHelper.SequenceWriteToBuffer(ipcS64Values, buffer);
		BinaryHelper.SaveToBuffer(ipcPropertyValue, buffer);
		BinaryHelper.SaveToBuffer(ipcRankValue, buffer);
		BinaryHelper.SaveToBuffer(ipcGsReadValue, buffer);
		BinaryHelper.SaveToBuffer(ipcGsWriteValue, buffer);
	}
};
