using System.Collections.Generic;

public enum GUILD_TITLE
{
	INVALID = -1,
	MASTER = 0,
	SUBMASTER,
	OFFICER,
	MEMBER,
	COUNT,
};

public class GuildApply : BinaryHelper.ISerializable
{
	public uint playerId;
	public uint guildId;
	public long applyTime;

	public static string GetTableName() {
		return "guild_apply";
	}
	public static string GetTableKeyName() {
		return string.Empty;
	}
	public uint GetTableKeyValue() {
		return (uint)0;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out playerId, buffer);
		BinaryHelper.ReadFromBuffer(out guildId, buffer);
		BinaryHelper.ReadFromBuffer(out applyTime, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(playerId, buffer);
		BinaryHelper.WriteToBuffer(guildId, buffer);
		BinaryHelper.WriteToBuffer(applyTime, buffer);
	}
};

public class GuildMember : BinaryHelper.ISerializable
{
	public uint playerId;
	public uint guildId;
	public sbyte guildTitle;
	public long joinTime;

	public static string GetTableName() {
		return "guild_member";
	}
	public static string GetTableKeyName() {
		return "playerId";
	}
	public uint GetTableKeyValue() {
		return (uint)playerId;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out playerId, buffer);
		BinaryHelper.ReadFromBuffer(out guildId, buffer);
		BinaryHelper.ReadFromBuffer(out guildTitle, buffer);
		BinaryHelper.ReadFromBuffer(out joinTime, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(playerId, buffer);
		BinaryHelper.WriteToBuffer(guildId, buffer);
		BinaryHelper.WriteToBuffer(guildTitle, buffer);
		BinaryHelper.WriteToBuffer(joinTime, buffer);
	}
};

public class GuildInformation : BinaryHelper.ISerializable
{
	public uint Id;
	public uint gsId;
	public string name;
	public long buildTime;
	public uint level;
	public long levelTime;

	public static string GetTableName() {
		return "guild";
	}
	public static string GetTableKeyName() {
		return "Id";
	}
	public uint GetTableKeyValue() {
		return (uint)Id;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out Id, buffer);
		BinaryHelper.ReadFromBuffer(out gsId, buffer);
		BinaryHelper.ReadFromBuffer(out name, buffer);
		BinaryHelper.ReadFromBuffer(out buildTime, buffer);
		BinaryHelper.ReadFromBuffer(out level, buffer);
		BinaryHelper.ReadFromBuffer(out levelTime, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(Id, buffer);
		BinaryHelper.WriteToBuffer(gsId, buffer);
		BinaryHelper.WriteToBuffer(name, buffer);
		BinaryHelper.WriteToBuffer(buildTime, buffer);
		BinaryHelper.WriteToBuffer(level, buffer);
		BinaryHelper.WriteToBuffer(levelTime, buffer);
	}
};
