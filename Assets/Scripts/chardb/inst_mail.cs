using System.Collections.Generic;

public enum MAIL_RQST_TYPE
{
	COUNT_ALL,
	LIST_SOMES,
	GET_ATTACHMENT,
	VIEW_DETAIL,
	WRITE_ORDER,
	DELETE_ORDER,

	SEND_SINGLE,
	SEND_MULTI_SAME,
	SEND_MULTI_DIFFERENT,
};

public enum MAIL_TYPE
{
	NORMAL,
	SYSTEM,
};

public enum MailFlag
{
	SubjectArgs = 1 << 0,
	BodyArgs = 1 << 1,
};

public class InstMailAttachment : BinaryHelper.ISerializable
{
	public uint mailID;
	public List<string> mailCheques;
	public List<string> mailItems;

	public static string GetTableName() {
		return "inst_mail";
	}
	public static string GetTableKeyName() {
		return "mailID";
	}
	public uint GetTableKeyValue() {
		return (uint)mailID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out mailID, buffer);
		BinaryHelper.SequenceReadFromBuffer(out mailCheques, buffer);
		BinaryHelper.SequenceReadFromBuffer(out mailItems, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(mailID, buffer);
		BinaryHelper.SequenceWriteToBuffer(mailCheques, buffer);
		BinaryHelper.SequenceWriteToBuffer(mailItems, buffer);
	}
};

public class inst_mail : BinaryHelper.ISerializable
{
	public uint mailID;
	public uint mailType;
	public uint mailFlags;
	public uint mailSender;
	public string mailSenderName;
	public uint mailReceiver;
	public long mailDeliverTime;
	public long mailExpireTime;
	public string mailSubject;
	public string mailBody;
	public List<string> mailCheques;
	public List<string> mailItems;
	public bool isGetAttachment;
	public bool isViewDetail;

	public static string GetTableName() {
		return "inst_mail";
	}
	public static string GetTableKeyName() {
		return "mailID";
	}
	public uint GetTableKeyValue() {
		return (uint)mailID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out mailID, buffer);
		BinaryHelper.ReadFromBuffer(out mailType, buffer);
		BinaryHelper.ReadFromBuffer(out mailFlags, buffer);
		BinaryHelper.ReadFromBuffer(out mailSender, buffer);
		BinaryHelper.ReadFromBuffer(out mailSenderName, buffer);
		BinaryHelper.ReadFromBuffer(out mailReceiver, buffer);
		BinaryHelper.ReadFromBuffer(out mailDeliverTime, buffer);
		BinaryHelper.ReadFromBuffer(out mailExpireTime, buffer);
		BinaryHelper.ReadFromBuffer(out mailSubject, buffer);
		BinaryHelper.ReadFromBuffer(out mailBody, buffer);
		BinaryHelper.SequenceReadFromBuffer(out mailCheques, buffer);
		BinaryHelper.SequenceReadFromBuffer(out mailItems, buffer);
		BinaryHelper.ReadFromBuffer(out isGetAttachment, buffer);
		BinaryHelper.ReadFromBuffer(out isViewDetail, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(mailID, buffer);
		BinaryHelper.WriteToBuffer(mailType, buffer);
		BinaryHelper.WriteToBuffer(mailFlags, buffer);
		BinaryHelper.WriteToBuffer(mailSender, buffer);
		BinaryHelper.WriteToBuffer(mailSenderName, buffer);
		BinaryHelper.WriteToBuffer(mailReceiver, buffer);
		BinaryHelper.WriteToBuffer(mailDeliverTime, buffer);
		BinaryHelper.WriteToBuffer(mailExpireTime, buffer);
		BinaryHelper.WriteToBuffer(mailSubject, buffer);
		BinaryHelper.WriteToBuffer(mailBody, buffer);
		BinaryHelper.SequenceWriteToBuffer(mailCheques, buffer);
		BinaryHelper.SequenceWriteToBuffer(mailItems, buffer);
		BinaryHelper.WriteToBuffer(isGetAttachment, buffer);
		BinaryHelper.WriteToBuffer(isViewDetail, buffer);
	}
};
