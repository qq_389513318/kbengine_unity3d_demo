using System.Collections.Generic;

public class inst_configure : BinaryHelper.ISerializable
{
	public uint cfgID;
	public string cfgValue;

	public static string GetTableName() {
		return "inst_configure";
	}
	public static string GetTableKeyName() {
		return "cfgID";
	}
	public uint GetTableKeyValue() {
		return (uint)cfgID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out cfgID, buffer);
		BinaryHelper.ReadFromBuffer(out cfgValue, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(cfgID, buffer);
		BinaryHelper.WriteToBuffer(cfgValue, buffer);
	}
};
