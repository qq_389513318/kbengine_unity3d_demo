using System.Collections.Generic;

public enum RANK_TYPE
{
	PLAYER,
	GUILD,
};

public enum RANK_SUBTYPE
{
	// player
	PLAYER_LEVEL = 0,
	PLAYER_FIGHT_VALUE,
	PLAYER_COUNT,

	// guild
	GUILD_LEVEL = 0,
	GUILD_COUNT,
};
