public class Utils
{
    static public void Swap<T>(ref T lhs, ref T rhs)
    {
        var x = lhs;
        lhs = rhs;
        rhs = x;
    }

    static public bool BIT_ISSET(ulong value, int pos)
    {
        return (value & ((ulong)1 << pos)) != 0;
    }

    static public uint ReadFromBuffer7BitEncodedInt(NetBuffer buffer)
    {
        uint value = 0;
        int count = 0;
        while (true)
        {
            buffer.Read(out byte bytes);
            value += (uint)(bytes & 0x7f) << (count++ * 7);
            if (bytes <= 0x7f)
            {
                break;
            }
        }
        return value;
    }

    static public void WriteToBuffer7BitEncodedInt(uint value, NetBuffer buffer)
    {
        while (true)
        {
            int flag = value > 0x7f ? 0x80 : 0;
            buffer.Write((byte)((uint)flag | value));
            if (flag > 0)
            {
                value >>= 7;
            }
            else
            {
                break;
            }
        }
    }
}
