using System.Collections.Generic;

public enum PlayerCareer
{
	None,
	Terran,
	Protoss,
	Demons,
	Max,
};

public enum PlayerGender
{
	None,
	Male,
	Female,
	Max,
};

public enum CurrencyType
{
	None,
	Gold,  // 金币
	Diamond,  // 钻石
	Max,
};

public enum ChequeType
{
	None,
	Gold,  // 金币
	Diamond,  // 钻石
	Exp,  // 经验
	Max,
};

public class Configure : BinaryHelper.ISerializable
{
	public uint cfgIndex;
	public string cfgName;
	public string cfgVal;

	public static string GetTableName() {
		return "configure";
	}
	public static string GetTableKeyName() {
		return string.Empty;
	}
	public uint GetTableKeyValue() {
		return (uint)0;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out cfgIndex, buffer);
		BinaryHelper.ReadFromBuffer(out cfgName, buffer);
		BinaryHelper.ReadFromBuffer(out cfgVal, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(cfgIndex, buffer);
		BinaryHelper.WriteToBuffer(cfgName, buffer);
		BinaryHelper.WriteToBuffer(cfgVal, buffer);
	}
};
