using System.Collections.Generic;

public class LootSet : BinaryHelper.ISerializable
{
	public uint lsID;
	public string lsName;

	public static string GetTableName() {
		return "loot_set";
	}
	public static string GetTableKeyName() {
		return "lsID";
	}
	public uint GetTableKeyValue() {
		return (uint)lsID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out lsID, buffer);
		BinaryHelper.ReadFromBuffer(out lsName, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(lsID, buffer);
		BinaryHelper.WriteToBuffer(lsName, buffer);
	}
};

public class LootSetGroup : BinaryHelper.ISerializable
{
	public uint lsgID;
	public uint lsID;
	public string lsgName;
	public float lsgOdds;
	public uint lsgMinItemTimes;
	public uint lsgMaxItemTimes;
	public uint lsgMinChequeTimes;
	public uint lsgMaxChequeTimes;

	public static string GetTableName() {
		return "loot_set_group";
	}
	public static string GetTableKeyName() {
		return "lsgID";
	}
	public uint GetTableKeyValue() {
		return (uint)lsgID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out lsgID, buffer);
		BinaryHelper.ReadFromBuffer(out lsID, buffer);
		BinaryHelper.ReadFromBuffer(out lsgName, buffer);
		BinaryHelper.ReadFromBuffer(out lsgOdds, buffer);
		BinaryHelper.ReadFromBuffer(out lsgMinItemTimes, buffer);
		BinaryHelper.ReadFromBuffer(out lsgMaxItemTimes, buffer);
		BinaryHelper.ReadFromBuffer(out lsgMinChequeTimes, buffer);
		BinaryHelper.ReadFromBuffer(out lsgMaxChequeTimes, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(lsgID, buffer);
		BinaryHelper.WriteToBuffer(lsID, buffer);
		BinaryHelper.WriteToBuffer(lsgName, buffer);
		BinaryHelper.WriteToBuffer(lsgOdds, buffer);
		BinaryHelper.WriteToBuffer(lsgMinItemTimes, buffer);
		BinaryHelper.WriteToBuffer(lsgMaxItemTimes, buffer);
		BinaryHelper.WriteToBuffer(lsgMinChequeTimes, buffer);
		BinaryHelper.WriteToBuffer(lsgMaxChequeTimes, buffer);
	}
};

public class LootSetGroupItem : BinaryHelper.ISerializable
{
	public class Flags : BinaryHelper.ISerializable
	{
		public bool bindToPicker;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out bindToPicker, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(bindToPicker, buffer);
		}
	};
	public uint lsgiID;
	public uint lsID;
	public uint lsgID;
	public Flags lsgiFlags;
	public uint lsgiWeight;
	public uint lsgiItemTypeID;
	public uint lsgiMinCount;
	public uint lsgiMaxCount;
	public uint lsgiMaxTimes;
	public uint lsgiLimitQuest;
	public uint lsgiLimitCareer;
	public uint lsgiLimitGender;

	public static string GetTableName() {
		return "loot_set_group_item";
	}
	public static string GetTableKeyName() {
		return "lsgiID";
	}
	public uint GetTableKeyValue() {
		return (uint)lsgiID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out lsgiID, buffer);
		BinaryHelper.ReadFromBuffer(out lsID, buffer);
		BinaryHelper.ReadFromBuffer(out lsgID, buffer);
		BinaryHelper.LoadFromBuffer(out lsgiFlags, buffer);
		BinaryHelper.ReadFromBuffer(out lsgiWeight, buffer);
		BinaryHelper.ReadFromBuffer(out lsgiItemTypeID, buffer);
		BinaryHelper.ReadFromBuffer(out lsgiMinCount, buffer);
		BinaryHelper.ReadFromBuffer(out lsgiMaxCount, buffer);
		BinaryHelper.ReadFromBuffer(out lsgiMaxTimes, buffer);
		BinaryHelper.ReadFromBuffer(out lsgiLimitQuest, buffer);
		BinaryHelper.ReadFromBuffer(out lsgiLimitCareer, buffer);
		BinaryHelper.ReadFromBuffer(out lsgiLimitGender, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(lsgiID, buffer);
		BinaryHelper.WriteToBuffer(lsID, buffer);
		BinaryHelper.WriteToBuffer(lsgID, buffer);
		BinaryHelper.SaveToBuffer(lsgiFlags, buffer);
		BinaryHelper.WriteToBuffer(lsgiWeight, buffer);
		BinaryHelper.WriteToBuffer(lsgiItemTypeID, buffer);
		BinaryHelper.WriteToBuffer(lsgiMinCount, buffer);
		BinaryHelper.WriteToBuffer(lsgiMaxCount, buffer);
		BinaryHelper.WriteToBuffer(lsgiMaxTimes, buffer);
		BinaryHelper.WriteToBuffer(lsgiLimitQuest, buffer);
		BinaryHelper.WriteToBuffer(lsgiLimitCareer, buffer);
		BinaryHelper.WriteToBuffer(lsgiLimitGender, buffer);
	}
};

public class LootSetGroupCheque : BinaryHelper.ISerializable
{
	public uint lsgcID;
	public uint lsID;
	public uint lsgID;
	public uint lsgcWeight;
	public uint lsgcChequeType;
	public uint lsgcMinValue;
	public uint lsgcMaxValue;
	public uint lsgcMaxTimes;

	public static string GetTableName() {
		return "loot_set_group_cheque";
	}
	public static string GetTableKeyName() {
		return "lsgcID";
	}
	public uint GetTableKeyValue() {
		return (uint)lsgcID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out lsgcID, buffer);
		BinaryHelper.ReadFromBuffer(out lsID, buffer);
		BinaryHelper.ReadFromBuffer(out lsgID, buffer);
		BinaryHelper.ReadFromBuffer(out lsgcWeight, buffer);
		BinaryHelper.ReadFromBuffer(out lsgcChequeType, buffer);
		BinaryHelper.ReadFromBuffer(out lsgcMinValue, buffer);
		BinaryHelper.ReadFromBuffer(out lsgcMaxValue, buffer);
		BinaryHelper.ReadFromBuffer(out lsgcMaxTimes, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(lsgcID, buffer);
		BinaryHelper.WriteToBuffer(lsID, buffer);
		BinaryHelper.WriteToBuffer(lsgID, buffer);
		BinaryHelper.WriteToBuffer(lsgcWeight, buffer);
		BinaryHelper.WriteToBuffer(lsgcChequeType, buffer);
		BinaryHelper.WriteToBuffer(lsgcMinValue, buffer);
		BinaryHelper.WriteToBuffer(lsgcMaxValue, buffer);
		BinaryHelper.WriteToBuffer(lsgcMaxTimes, buffer);
	}
};
