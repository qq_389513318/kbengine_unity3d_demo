using System.Collections.Generic;

public enum CharRaceType
{
	Player,
	Neutral,
	HostileForces,
	FriendlyForces,
};

public enum CharEliteType
{
	Normal,
	Boss,
};

public class CharPrototype : BinaryHelper.ISerializable
{
	public class Flags : BinaryHelper.ISerializable
	{
		public bool isUndead;
		public bool isJoinCombat;
		public bool isFastTurn;
		public bool isKeepDir;
		public bool isActivity;
		public bool isIgnoreInvisible;
		public bool isSenseCreature;
		public bool isSensePlayer;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out isUndead, buffer);
			BinaryHelper.ReadFromBuffer(out isJoinCombat, buffer);
			BinaryHelper.ReadFromBuffer(out isFastTurn, buffer);
			BinaryHelper.ReadFromBuffer(out isKeepDir, buffer);
			BinaryHelper.ReadFromBuffer(out isActivity, buffer);
			BinaryHelper.ReadFromBuffer(out isIgnoreInvisible, buffer);
			BinaryHelper.ReadFromBuffer(out isSenseCreature, buffer);
			BinaryHelper.ReadFromBuffer(out isSensePlayer, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(isUndead, buffer);
			BinaryHelper.WriteToBuffer(isJoinCombat, buffer);
			BinaryHelper.WriteToBuffer(isFastTurn, buffer);
			BinaryHelper.WriteToBuffer(isKeepDir, buffer);
			BinaryHelper.WriteToBuffer(isActivity, buffer);
			BinaryHelper.WriteToBuffer(isIgnoreInvisible, buffer);
			BinaryHelper.WriteToBuffer(isSenseCreature, buffer);
			BinaryHelper.WriteToBuffer(isSensePlayer, buffer);
		}
	};

	public uint charTypeId;
	public Flags charFlags;
	public uint charRace;
	public uint charElite;
	public uint level;

	public float speedWalk;
	public float speedRun;
	public float speedTurn;

	public float patrolRange;
	public float boundRadius;
	public float senseRadius;
	public float combatRadius;

	public float combatBestDist;

	public uint minRespawnTime;
	public uint maxRespawnTime;
	public uint deadDisappearTime;

	public double recoveryHPRate;
	public double recoveryHPValue;
	public double recoveryMPRate;
	public double recoveryMPValue;

	public uint lootSetID;

	public uint aiScriptId;
	public uint spawnScriptId;
	public string spawnScriptArgs;
	public uint deadScriptId;
	public string deadScriptArgs;
	public uint playScriptId;
	public string playScriptArgs;

	public static string GetTableName() {
		return "char_prototype";
	}
	public static string GetTableKeyName() {
		return "charTypeId";
	}
	public uint GetTableKeyValue() {
		return (uint)charTypeId;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out charTypeId, buffer);
		BinaryHelper.LoadFromBuffer(out charFlags, buffer);
		BinaryHelper.ReadFromBuffer(out charRace, buffer);
		BinaryHelper.ReadFromBuffer(out charElite, buffer);
		BinaryHelper.ReadFromBuffer(out level, buffer);
		BinaryHelper.ReadFromBuffer(out speedWalk, buffer);
		BinaryHelper.ReadFromBuffer(out speedRun, buffer);
		BinaryHelper.ReadFromBuffer(out speedTurn, buffer);
		BinaryHelper.ReadFromBuffer(out patrolRange, buffer);
		BinaryHelper.ReadFromBuffer(out boundRadius, buffer);
		BinaryHelper.ReadFromBuffer(out senseRadius, buffer);
		BinaryHelper.ReadFromBuffer(out combatRadius, buffer);
		BinaryHelper.ReadFromBuffer(out combatBestDist, buffer);
		BinaryHelper.ReadFromBuffer(out minRespawnTime, buffer);
		BinaryHelper.ReadFromBuffer(out maxRespawnTime, buffer);
		BinaryHelper.ReadFromBuffer(out deadDisappearTime, buffer);
		BinaryHelper.ReadFromBuffer(out recoveryHPRate, buffer);
		BinaryHelper.ReadFromBuffer(out recoveryHPValue, buffer);
		BinaryHelper.ReadFromBuffer(out recoveryMPRate, buffer);
		BinaryHelper.ReadFromBuffer(out recoveryMPValue, buffer);
		BinaryHelper.ReadFromBuffer(out lootSetID, buffer);
		BinaryHelper.ReadFromBuffer(out aiScriptId, buffer);
		BinaryHelper.ReadFromBuffer(out spawnScriptId, buffer);
		BinaryHelper.ReadFromBuffer(out spawnScriptArgs, buffer);
		BinaryHelper.ReadFromBuffer(out deadScriptId, buffer);
		BinaryHelper.ReadFromBuffer(out deadScriptArgs, buffer);
		BinaryHelper.ReadFromBuffer(out playScriptId, buffer);
		BinaryHelper.ReadFromBuffer(out playScriptArgs, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(charTypeId, buffer);
		BinaryHelper.SaveToBuffer(charFlags, buffer);
		BinaryHelper.WriteToBuffer(charRace, buffer);
		BinaryHelper.WriteToBuffer(charElite, buffer);
		BinaryHelper.WriteToBuffer(level, buffer);
		BinaryHelper.WriteToBuffer(speedWalk, buffer);
		BinaryHelper.WriteToBuffer(speedRun, buffer);
		BinaryHelper.WriteToBuffer(speedTurn, buffer);
		BinaryHelper.WriteToBuffer(patrolRange, buffer);
		BinaryHelper.WriteToBuffer(boundRadius, buffer);
		BinaryHelper.WriteToBuffer(senseRadius, buffer);
		BinaryHelper.WriteToBuffer(combatRadius, buffer);
		BinaryHelper.WriteToBuffer(combatBestDist, buffer);
		BinaryHelper.WriteToBuffer(minRespawnTime, buffer);
		BinaryHelper.WriteToBuffer(maxRespawnTime, buffer);
		BinaryHelper.WriteToBuffer(deadDisappearTime, buffer);
		BinaryHelper.WriteToBuffer(recoveryHPRate, buffer);
		BinaryHelper.WriteToBuffer(recoveryHPValue, buffer);
		BinaryHelper.WriteToBuffer(recoveryMPRate, buffer);
		BinaryHelper.WriteToBuffer(recoveryMPValue, buffer);
		BinaryHelper.WriteToBuffer(lootSetID, buffer);
		BinaryHelper.WriteToBuffer(aiScriptId, buffer);
		BinaryHelper.WriteToBuffer(spawnScriptId, buffer);
		BinaryHelper.WriteToBuffer(spawnScriptArgs, buffer);
		BinaryHelper.WriteToBuffer(deadScriptId, buffer);
		BinaryHelper.WriteToBuffer(deadScriptArgs, buffer);
		BinaryHelper.WriteToBuffer(playScriptId, buffer);
		BinaryHelper.WriteToBuffer(playScriptArgs, buffer);
	}
};


public class CreatureSpawn : BinaryHelper.ISerializable
{
	public class Flags : BinaryHelper.ISerializable
	{
		public bool isRespawn;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out isRespawn, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(isRespawn, buffer);
		}
	};
	public uint spawnId;
	public Flags flags;
	public uint entry;
	public uint level;
	public uint map_id;
	public uint map_type;
	public float x, y, z, o;
	public uint idleType;
	public uint wayPointId;

	public static string GetTableName() {
		return "creature_spawn";
	}
	public static string GetTableKeyName() {
		return "spawnId";
	}
	public uint GetTableKeyValue() {
		return (uint)spawnId;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out spawnId, buffer);
		BinaryHelper.LoadFromBuffer(out flags, buffer);
		BinaryHelper.ReadFromBuffer(out entry, buffer);
		BinaryHelper.ReadFromBuffer(out level, buffer);
		BinaryHelper.ReadFromBuffer(out map_id, buffer);
		BinaryHelper.ReadFromBuffer(out map_type, buffer);
		BinaryHelper.ReadFromBuffer(out x, buffer);
		BinaryHelper.ReadFromBuffer(out y, buffer);
		BinaryHelper.ReadFromBuffer(out z, buffer);
		BinaryHelper.ReadFromBuffer(out o, buffer);
		BinaryHelper.ReadFromBuffer(out idleType, buffer);
		BinaryHelper.ReadFromBuffer(out wayPointId, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(spawnId, buffer);
		BinaryHelper.SaveToBuffer(flags, buffer);
		BinaryHelper.WriteToBuffer(entry, buffer);
		BinaryHelper.WriteToBuffer(level, buffer);
		BinaryHelper.WriteToBuffer(map_id, buffer);
		BinaryHelper.WriteToBuffer(map_type, buffer);
		BinaryHelper.WriteToBuffer(x, buffer);
		BinaryHelper.WriteToBuffer(y, buffer);
		BinaryHelper.WriteToBuffer(z, buffer);
		BinaryHelper.WriteToBuffer(o, buffer);
		BinaryHelper.WriteToBuffer(idleType, buffer);
		BinaryHelper.WriteToBuffer(wayPointId, buffer);
	}
};
