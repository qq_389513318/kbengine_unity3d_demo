using System.Collections.Generic;

public enum ATTRARITHTYPE
{
	BASE,
	SCALE,
	FINAL,
	COUNT,
};

public enum ATTRTYPE
{
	NONE,

	HIT_POINT,
	MAGIC_POINT,

	ATTACK_VALUE,
	DEFENSE_VALUE,

	HIT_CHANCE,
	DODGE_CHANCE,
	CRITIHIT_CHANCE,
	CRITIHIT_CHANCE_RESIST,
	CRITIHIT_INTENSITY,
	CRITIHIT_INTENSITY_RESIST,

	COUNT,
};

public class PlayerBase : BinaryHelper.ISerializable
{
	public uint level;
	public ulong lvUpExp;
	public double damageFactor;
	public double recoveryHPRate;
	public double recoveryHPValue;
	public double recoveryMPRate;
	public double recoveryMPValue;

	public static string GetTableName() {
		return "player_base";
	}
	public static string GetTableKeyName() {
		return "level";
	}
	public uint GetTableKeyValue() {
		return (uint)level;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out level, buffer);
		BinaryHelper.ReadFromBuffer(out lvUpExp, buffer);
		BinaryHelper.ReadFromBuffer(out damageFactor, buffer);
		BinaryHelper.ReadFromBuffer(out recoveryHPRate, buffer);
		BinaryHelper.ReadFromBuffer(out recoveryHPValue, buffer);
		BinaryHelper.ReadFromBuffer(out recoveryMPRate, buffer);
		BinaryHelper.ReadFromBuffer(out recoveryMPValue, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(level, buffer);
		BinaryHelper.WriteToBuffer(lvUpExp, buffer);
		BinaryHelper.WriteToBuffer(damageFactor, buffer);
		BinaryHelper.WriteToBuffer(recoveryHPRate, buffer);
		BinaryHelper.WriteToBuffer(recoveryHPValue, buffer);
		BinaryHelper.WriteToBuffer(recoveryMPRate, buffer);
		BinaryHelper.WriteToBuffer(recoveryMPValue, buffer);
	}
};

public class PlayerAttribute : BinaryHelper.ISerializable
{
	public uint Id;
	public uint career;
	public uint level;
	public List<double> attrs;

	public static string GetTableName() {
		return "player_attribute";
	}
	public static string GetTableKeyName() {
		return "Id";
	}
	public uint GetTableKeyValue() {
		return (uint)Id;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out Id, buffer);
		BinaryHelper.ReadFromBuffer(out career, buffer);
		BinaryHelper.ReadFromBuffer(out level, buffer);
		BinaryHelper.SequenceReadFromBuffer(out attrs, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(Id, buffer);
		BinaryHelper.WriteToBuffer(career, buffer);
		BinaryHelper.WriteToBuffer(level, buffer);
		BinaryHelper.SequenceWriteToBuffer(attrs, buffer);
	}
};

public class CreatureAttribute : BinaryHelper.ISerializable
{
	public uint Id;
	public uint elite;
	public uint level;
	public List<double> attrs;
	public double damageFactor;

	public static string GetTableName() {
		return "creature_attribute";
	}
	public static string GetTableKeyName() {
		return "Id";
	}
	public uint GetTableKeyValue() {
		return (uint)Id;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out Id, buffer);
		BinaryHelper.ReadFromBuffer(out elite, buffer);
		BinaryHelper.ReadFromBuffer(out level, buffer);
		BinaryHelper.SequenceReadFromBuffer(out attrs, buffer);
		BinaryHelper.ReadFromBuffer(out damageFactor, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(Id, buffer);
		BinaryHelper.WriteToBuffer(elite, buffer);
		BinaryHelper.WriteToBuffer(level, buffer);
		BinaryHelper.SequenceWriteToBuffer(attrs, buffer);
		BinaryHelper.WriteToBuffer(damageFactor, buffer);
	}
};

public class CreatureCustomAttribute : BinaryHelper.ISerializable
{
	public uint charTypeId;
	public List<double> attrs;
	public double damageFactor;

	public static string GetTableName() {
		return "creature_custom_attribute";
	}
	public static string GetTableKeyName() {
		return "charTypeId";
	}
	public uint GetTableKeyValue() {
		return (uint)charTypeId;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out charTypeId, buffer);
		BinaryHelper.SequenceReadFromBuffer(out attrs, buffer);
		BinaryHelper.ReadFromBuffer(out damageFactor, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(charTypeId, buffer);
		BinaryHelper.SequenceWriteToBuffer(attrs, buffer);
		BinaryHelper.WriteToBuffer(damageFactor, buffer);
	}
};
