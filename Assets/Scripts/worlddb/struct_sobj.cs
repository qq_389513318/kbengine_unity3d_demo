using System.Collections.Generic;

public class SObjPrototype : BinaryHelper.ISerializable
{
	public class Flags : BinaryHelper.ISerializable
	{
		public bool isExclusiveMine;
		public bool isRemoveAfterMineDone;
		public bool isPlayerInteresting;
		public bool isCreatureInteresting;
		public bool isActivity;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out isExclusiveMine, buffer);
			BinaryHelper.ReadFromBuffer(out isRemoveAfterMineDone, buffer);
			BinaryHelper.ReadFromBuffer(out isPlayerInteresting, buffer);
			BinaryHelper.ReadFromBuffer(out isCreatureInteresting, buffer);
			BinaryHelper.ReadFromBuffer(out isActivity, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(isExclusiveMine, buffer);
			BinaryHelper.WriteToBuffer(isRemoveAfterMineDone, buffer);
			BinaryHelper.WriteToBuffer(isPlayerInteresting, buffer);
			BinaryHelper.WriteToBuffer(isCreatureInteresting, buffer);
			BinaryHelper.WriteToBuffer(isActivity, buffer);
		}
	};
	public class CostItem : BinaryHelper.ISerializable
	{
		public uint itemId;
		public uint itemNum;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out itemId, buffer);
			BinaryHelper.ReadFromBuffer(out itemNum, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(itemId, buffer);
			BinaryHelper.WriteToBuffer(itemNum, buffer);
		}
	};

	public uint sobjTypeId;
	public Flags sobjFlags;

	public uint minRespawnTime;
	public uint maxRespawnTime;

	public uint teleportPointID;
	public uint teleportDelayTime;

	public uint lootSetID;
	public uint mineSpellID;
	public uint mineSpellLv;
	public uint mineAnimTime;
	public List<CostItem> mineCostItems;

	public uint sobjReqQuestDoing;

	public float radius;

	public uint spawnScriptId;
	public string spawnScriptArgs;
	public uint playScriptId;
	public string playScriptArgs;

	public static string GetTableName() {
		return "sobj_prototype";
	}
	public static string GetTableKeyName() {
		return "sobjTypeId";
	}
	public uint GetTableKeyValue() {
		return (uint)sobjTypeId;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out sobjTypeId, buffer);
		BinaryHelper.LoadFromBuffer(out sobjFlags, buffer);
		BinaryHelper.ReadFromBuffer(out minRespawnTime, buffer);
		BinaryHelper.ReadFromBuffer(out maxRespawnTime, buffer);
		BinaryHelper.ReadFromBuffer(out teleportPointID, buffer);
		BinaryHelper.ReadFromBuffer(out teleportDelayTime, buffer);
		BinaryHelper.ReadFromBuffer(out lootSetID, buffer);
		BinaryHelper.ReadFromBuffer(out mineSpellID, buffer);
		BinaryHelper.ReadFromBuffer(out mineSpellLv, buffer);
		BinaryHelper.ReadFromBuffer(out mineAnimTime, buffer);
		BinaryHelper.BlockSequenceReadFromBuffer(out mineCostItems, buffer);
		BinaryHelper.ReadFromBuffer(out sobjReqQuestDoing, buffer);
		BinaryHelper.ReadFromBuffer(out radius, buffer);
		BinaryHelper.ReadFromBuffer(out spawnScriptId, buffer);
		BinaryHelper.ReadFromBuffer(out spawnScriptArgs, buffer);
		BinaryHelper.ReadFromBuffer(out playScriptId, buffer);
		BinaryHelper.ReadFromBuffer(out playScriptArgs, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(sobjTypeId, buffer);
		BinaryHelper.SaveToBuffer(sobjFlags, buffer);
		BinaryHelper.WriteToBuffer(minRespawnTime, buffer);
		BinaryHelper.WriteToBuffer(maxRespawnTime, buffer);
		BinaryHelper.WriteToBuffer(teleportPointID, buffer);
		BinaryHelper.WriteToBuffer(teleportDelayTime, buffer);
		BinaryHelper.WriteToBuffer(lootSetID, buffer);
		BinaryHelper.WriteToBuffer(mineSpellID, buffer);
		BinaryHelper.WriteToBuffer(mineSpellLv, buffer);
		BinaryHelper.WriteToBuffer(mineAnimTime, buffer);
		BinaryHelper.BlockSequenceWriteToBuffer(mineCostItems, buffer);
		BinaryHelper.WriteToBuffer(sobjReqQuestDoing, buffer);
		BinaryHelper.WriteToBuffer(radius, buffer);
		BinaryHelper.WriteToBuffer(spawnScriptId, buffer);
		BinaryHelper.WriteToBuffer(spawnScriptArgs, buffer);
		BinaryHelper.WriteToBuffer(playScriptId, buffer);
		BinaryHelper.WriteToBuffer(playScriptArgs, buffer);
	}
};


public class StaticObjectSpawn : BinaryHelper.ISerializable
{
	public class Flags : BinaryHelper.ISerializable
	{
		public bool isRespawn;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out isRespawn, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(isRespawn, buffer);
		}
	};
	public uint spawnId;
	public Flags flags;
	public uint entry;
	public uint map_id;
	public uint map_type;
	public float x, y, z, o;
	public float radius;

	public static string GetTableName() {
		return "staticobject_spawn";
	}
	public static string GetTableKeyName() {
		return "spawnId";
	}
	public uint GetTableKeyValue() {
		return (uint)spawnId;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out spawnId, buffer);
		BinaryHelper.LoadFromBuffer(out flags, buffer);
		BinaryHelper.ReadFromBuffer(out entry, buffer);
		BinaryHelper.ReadFromBuffer(out map_id, buffer);
		BinaryHelper.ReadFromBuffer(out map_type, buffer);
		BinaryHelper.ReadFromBuffer(out x, buffer);
		BinaryHelper.ReadFromBuffer(out y, buffer);
		BinaryHelper.ReadFromBuffer(out z, buffer);
		BinaryHelper.ReadFromBuffer(out o, buffer);
		BinaryHelper.ReadFromBuffer(out radius, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(spawnId, buffer);
		BinaryHelper.SaveToBuffer(flags, buffer);
		BinaryHelper.WriteToBuffer(entry, buffer);
		BinaryHelper.WriteToBuffer(map_id, buffer);
		BinaryHelper.WriteToBuffer(map_type, buffer);
		BinaryHelper.WriteToBuffer(x, buffer);
		BinaryHelper.WriteToBuffer(y, buffer);
		BinaryHelper.WriteToBuffer(z, buffer);
		BinaryHelper.WriteToBuffer(o, buffer);
		BinaryHelper.WriteToBuffer(radius, buffer);
	}
};
