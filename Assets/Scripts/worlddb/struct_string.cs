using System.Collections.Generic;

public enum STRING_TEXT_TYPE
{
	SYS_MSG,
	CLIENT_TEXT,
	ERROR_TEXT,
	MAP_NAME,
	ZONE_NAME,
	CHAR_NAME,
	CHAR_DESC,
	SOBJ_NAME,
	SOBJ_DESC,
	QUEST_NAME,
	QUEST_DESC,
	QUEST_INTRO,
	QUEST_DONE,
	QUEST_NOT_DONE,
	QUEST_CUSTOMIZE,
	ITEM_NAME,
	ITEM_DESC,
	SPELL_NAME,
	SPELL_DESC,
	BUFF_DESC,
	QUESTION,
};

public class string_text_list : BinaryHelper.ISerializable
{
	public uint stringID;
	public string stringEN;
	public string stringCN;

	public static string GetTableName() {
		return "string_text_list";
	}
	public static string GetTableKeyName() {
		return "stringID";
	}
	public uint GetTableKeyValue() {
		return (uint)stringID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out stringID, buffer);
		BinaryHelper.ReadFromBuffer(out stringEN, buffer);
		BinaryHelper.ReadFromBuffer(out stringCN, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(stringID, buffer);
		BinaryHelper.WriteToBuffer(stringEN, buffer);
		BinaryHelper.WriteToBuffer(stringCN, buffer);
	}
};
