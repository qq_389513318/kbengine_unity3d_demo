using System.Collections.Generic;

public class EditorTreeView : BinaryHelper.ISerializable
{
	public enum Type
	{
		None,
		Spell,
		Quest,
		Char,
		SObj,
		Item,
		Loot,
	};
	public uint etvType;
	public string etvTree;

	public static string GetTableName() {
		return "editor_tree_view";
	}
	public static string GetTableKeyName() {
		return "etvType";
	}
	public uint GetTableKeyValue() {
		return (uint)etvType;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out etvType, buffer);
		BinaryHelper.ReadFromBuffer(out etvTree, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(etvType, buffer);
		BinaryHelper.WriteToBuffer(etvTree, buffer);
	}
};
