using System.Collections.Generic;

public enum ItemClass
{
	None,
	Equip,
	Quest,
	Material,
	Gemstone,
	Consumable,
	Scrap,
	Other,
};

public enum ItemSubClass
{
	Equip_Weapon = 1,
	Equip_Belt,
	Equip_Gloves,
	Equip_Shoes,
};

public enum ItemQuality
{
	White,
	Red,
	Count,
};

public class ItemPrototype : BinaryHelper.ISerializable
{
	public class Flags : BinaryHelper.ISerializable
	{
		public bool canUse;
		public bool canSell;
		public bool canDestroy;
		public bool isDestroyAfterUse;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out canUse, buffer);
			BinaryHelper.ReadFromBuffer(out canSell, buffer);
			BinaryHelper.ReadFromBuffer(out canDestroy, buffer);
			BinaryHelper.ReadFromBuffer(out isDestroyAfterUse, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(canUse, buffer);
			BinaryHelper.WriteToBuffer(canSell, buffer);
			BinaryHelper.WriteToBuffer(canDestroy, buffer);
			BinaryHelper.WriteToBuffer(isDestroyAfterUse, buffer);
		}
	};

	public uint itemTypeID;
	public Flags itemFlags;
	public uint itemClass;
	public uint itemSubClass;
	public uint itemQuality;
	public uint itemLevel;
	public uint itemStack;

	public uint itemSellPrice;

	public uint itemLootId;
	public uint itemSpellId;
	public uint itemSpellLevel;
	public uint itemScriptId;
	public string itemScriptArgs;

	public static string GetTableName() {
		return "item_prototype";
	}
	public static string GetTableKeyName() {
		return "itemTypeID";
	}
	public uint GetTableKeyValue() {
		return (uint)itemTypeID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out itemTypeID, buffer);
		BinaryHelper.LoadFromBuffer(out itemFlags, buffer);
		BinaryHelper.ReadFromBuffer(out itemClass, buffer);
		BinaryHelper.ReadFromBuffer(out itemSubClass, buffer);
		BinaryHelper.ReadFromBuffer(out itemQuality, buffer);
		BinaryHelper.ReadFromBuffer(out itemLevel, buffer);
		BinaryHelper.ReadFromBuffer(out itemStack, buffer);
		BinaryHelper.ReadFromBuffer(out itemSellPrice, buffer);
		BinaryHelper.ReadFromBuffer(out itemLootId, buffer);
		BinaryHelper.ReadFromBuffer(out itemSpellId, buffer);
		BinaryHelper.ReadFromBuffer(out itemSpellLevel, buffer);
		BinaryHelper.ReadFromBuffer(out itemScriptId, buffer);
		BinaryHelper.ReadFromBuffer(out itemScriptArgs, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(itemTypeID, buffer);
		BinaryHelper.SaveToBuffer(itemFlags, buffer);
		BinaryHelper.WriteToBuffer(itemClass, buffer);
		BinaryHelper.WriteToBuffer(itemSubClass, buffer);
		BinaryHelper.WriteToBuffer(itemQuality, buffer);
		BinaryHelper.WriteToBuffer(itemLevel, buffer);
		BinaryHelper.WriteToBuffer(itemStack, buffer);
		BinaryHelper.WriteToBuffer(itemSellPrice, buffer);
		BinaryHelper.WriteToBuffer(itemLootId, buffer);
		BinaryHelper.WriteToBuffer(itemSpellId, buffer);
		BinaryHelper.WriteToBuffer(itemSpellLevel, buffer);
		BinaryHelper.WriteToBuffer(itemScriptId, buffer);
		BinaryHelper.WriteToBuffer(itemScriptArgs, buffer);
	}
};


public class ItemEquipPrototype : BinaryHelper.ISerializable
{
	public uint itemTypeID;

	public class Attr : BinaryHelper.ISerializable
	{
		public uint type;
		public double value;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out type, buffer);
			BinaryHelper.ReadFromBuffer(out value, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(type, buffer);
			BinaryHelper.WriteToBuffer(value, buffer);
		}
	};
	public List<Attr> itemEquipAttrs;

	public class Spell : BinaryHelper.ISerializable
	{
		public uint id;
		public uint level;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out id, buffer);
			BinaryHelper.ReadFromBuffer(out level, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(id, buffer);
			BinaryHelper.WriteToBuffer(level, buffer);
		}
	};
	public List<Spell> itemEquipSpells;

	public static string GetTableName() {
		return "item_equip_prototype";
	}
	public static string GetTableKeyName() {
		return "itemTypeID";
	}
	public uint GetTableKeyValue() {
		return (uint)itemTypeID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out itemTypeID, buffer);
		BinaryHelper.BlockSequenceReadFromBuffer(out itemEquipAttrs, buffer);
		BinaryHelper.BlockSequenceReadFromBuffer(out itemEquipSpells, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(itemTypeID, buffer);
		BinaryHelper.BlockSequenceWriteToBuffer(itemEquipAttrs, buffer);
		BinaryHelper.BlockSequenceWriteToBuffer(itemEquipSpells, buffer);
	}
};
