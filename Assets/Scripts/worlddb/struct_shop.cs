using System.Collections.Generic;

public class ShopPrototype : BinaryHelper.ISerializable
{
	public class Flags : BinaryHelper.ISerializable
	{
		public bool isBinding;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out isBinding, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(isBinding, buffer);
		}
	};

	public uint Id;
	public uint shopType;
	public Flags itemFlags;
	public uint itemTypeID;
	public uint itemCount;
	public uint currencyType;
	public uint sellPrice;
	public uint discountPrice;
	public uint bundleNumMax;
	public uint genderLimit;
	public uint careerLimit;
	public uint dailyBuyLimit;
	public uint weeklyBuyLimit;

	public static string GetTableName() {
		return "shop_prototype";
	}
	public static string GetTableKeyName() {
		return "Id";
	}
	public uint GetTableKeyValue() {
		return (uint)Id;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out Id, buffer);
		BinaryHelper.ReadFromBuffer(out shopType, buffer);
		BinaryHelper.LoadFromBuffer(out itemFlags, buffer);
		BinaryHelper.ReadFromBuffer(out itemTypeID, buffer);
		BinaryHelper.ReadFromBuffer(out itemCount, buffer);
		BinaryHelper.ReadFromBuffer(out currencyType, buffer);
		BinaryHelper.ReadFromBuffer(out sellPrice, buffer);
		BinaryHelper.ReadFromBuffer(out discountPrice, buffer);
		BinaryHelper.ReadFromBuffer(out bundleNumMax, buffer);
		BinaryHelper.ReadFromBuffer(out genderLimit, buffer);
		BinaryHelper.ReadFromBuffer(out careerLimit, buffer);
		BinaryHelper.ReadFromBuffer(out dailyBuyLimit, buffer);
		BinaryHelper.ReadFromBuffer(out weeklyBuyLimit, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(Id, buffer);
		BinaryHelper.WriteToBuffer(shopType, buffer);
		BinaryHelper.SaveToBuffer(itemFlags, buffer);
		BinaryHelper.WriteToBuffer(itemTypeID, buffer);
		BinaryHelper.WriteToBuffer(itemCount, buffer);
		BinaryHelper.WriteToBuffer(currencyType, buffer);
		BinaryHelper.WriteToBuffer(sellPrice, buffer);
		BinaryHelper.WriteToBuffer(discountPrice, buffer);
		BinaryHelper.WriteToBuffer(bundleNumMax, buffer);
		BinaryHelper.WriteToBuffer(genderLimit, buffer);
		BinaryHelper.WriteToBuffer(careerLimit, buffer);
		BinaryHelper.WriteToBuffer(dailyBuyLimit, buffer);
		BinaryHelper.WriteToBuffer(weeklyBuyLimit, buffer);
	}
};

public class SpecialShopPrototype : ShopPrototype
{
	public ulong itemUniqueKey;
	public long itemBeginTime;
	public long itemEndTime;
	public uint serverBuyLimit;
	public uint charBuyLimit;
	public uint dailyServerBuyLimit;
	public uint weeklyServerBuyLimit;

	public override void LoadFromBuffer(NetBuffer buffer) {
		base.LoadFromBuffer(buffer);
		BinaryHelper.ReadFromBuffer(out itemUniqueKey, buffer);
		BinaryHelper.ReadFromBuffer(out itemBeginTime, buffer);
		BinaryHelper.ReadFromBuffer(out itemEndTime, buffer);
		BinaryHelper.ReadFromBuffer(out serverBuyLimit, buffer);
		BinaryHelper.ReadFromBuffer(out charBuyLimit, buffer);
		BinaryHelper.ReadFromBuffer(out dailyServerBuyLimit, buffer);
		BinaryHelper.ReadFromBuffer(out weeklyServerBuyLimit, buffer);
	}
	public override void SaveToBuffer(NetBuffer buffer) {
		base.SaveToBuffer(buffer);
		BinaryHelper.WriteToBuffer(itemUniqueKey, buffer);
		BinaryHelper.WriteToBuffer(itemBeginTime, buffer);
		BinaryHelper.WriteToBuffer(itemEndTime, buffer);
		BinaryHelper.WriteToBuffer(serverBuyLimit, buffer);
		BinaryHelper.WriteToBuffer(charBuyLimit, buffer);
		BinaryHelper.WriteToBuffer(dailyServerBuyLimit, buffer);
		BinaryHelper.WriteToBuffer(weeklyServerBuyLimit, buffer);
	}
};
