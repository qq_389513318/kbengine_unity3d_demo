using System.Collections.Generic;

public enum QuestNavIdx
{
	Publisher = -1,
	Verifier = -2,
	Guider = -3,
};

public enum QuestClassType
{
	None,
	MainLine,
	BranchLine,
};

public enum QuestRepeatType
{
	None,
	Daily,
	Weekly,
	Monthly,
	Forever,
	Count,
};

public enum QuestWhenType
{
	Accept,
	Finish,
	Failed,
	Cancel,
	Submit,
	Count,
};

public enum QuestObjType
{
	None,
	Guide,
	NPC,
	SObj,
	NPCPt,
	SObjPt,
	Count,
};

public class QuestObjInst : BinaryHelper.ISerializable
{
	public uint objType;
	public uint objID;

	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out objType, buffer);
		BinaryHelper.ReadFromBuffer(out objID, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(objType, buffer);
		BinaryHelper.WriteToBuffer(objID, buffer);
	}
};

public class QuestScript : BinaryHelper.ISerializable
{
	public uint scriptID;
	public string scriptArgs;

	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out scriptID, buffer);
		BinaryHelper.ReadFromBuffer(out scriptArgs, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(scriptID, buffer);
		BinaryHelper.WriteToBuffer(scriptArgs, buffer);
	}
};

public class QuestCheque : BinaryHelper.ISerializable
{
	public uint chequeType;
	public uint chequeValue;

	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out chequeType, buffer);
		BinaryHelper.ReadFromBuffer(out chequeValue, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(chequeType, buffer);
		BinaryHelper.WriteToBuffer(chequeValue, buffer);
	}
};

public class QuestItem : BinaryHelper.ISerializable
{
	public uint itemTypeID;
	public uint itemCount;
	public uint onlyCareer;
	public uint onlyGender;

	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out itemTypeID, buffer);
		BinaryHelper.ReadFromBuffer(out itemCount, buffer);
		BinaryHelper.ReadFromBuffer(out onlyCareer, buffer);
		BinaryHelper.ReadFromBuffer(out onlyGender, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(itemTypeID, buffer);
		BinaryHelper.WriteToBuffer(itemCount, buffer);
		BinaryHelper.WriteToBuffer(onlyCareer, buffer);
		BinaryHelper.WriteToBuffer(onlyGender, buffer);
	}
};

public class QuestChequeReq : QuestCheque
{
	public bool isCost;
	public bool isRefund;

	public override void LoadFromBuffer(NetBuffer buffer) {
		base.LoadFromBuffer(buffer);
		BinaryHelper.ReadFromBuffer(out isCost, buffer);
		BinaryHelper.ReadFromBuffer(out isRefund, buffer);
	}
	public override void SaveToBuffer(NetBuffer buffer) {
		base.SaveToBuffer(buffer);
		BinaryHelper.WriteToBuffer(isCost, buffer);
		BinaryHelper.WriteToBuffer(isRefund, buffer);
	}
};

public class QuestItemReq : QuestItem
{
	public bool isDestroy;
	public bool isRefund;
	public bool isBinding;

	public override void LoadFromBuffer(NetBuffer buffer) {
		base.LoadFromBuffer(buffer);
		BinaryHelper.ReadFromBuffer(out isDestroy, buffer);
		BinaryHelper.ReadFromBuffer(out isRefund, buffer);
		BinaryHelper.ReadFromBuffer(out isBinding, buffer);
	}
	public override void SaveToBuffer(NetBuffer buffer) {
		base.SaveToBuffer(buffer);
		BinaryHelper.WriteToBuffer(isDestroy, buffer);
		BinaryHelper.WriteToBuffer(isRefund, buffer);
		BinaryHelper.WriteToBuffer(isBinding, buffer);
	}
};

public class QuestQuestionReq : BinaryHelper.ISerializable
{
	public uint questionId;
	public byte optionId;

	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out questionId, buffer);
		BinaryHelper.ReadFromBuffer(out optionId, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(questionId, buffer);
		BinaryHelper.WriteToBuffer(optionId, buffer);
	}
};

public class QuestChequeInit : QuestCheque
{
	public bool isRetrieve;

	public override void LoadFromBuffer(NetBuffer buffer) {
		base.LoadFromBuffer(buffer);
		BinaryHelper.ReadFromBuffer(out isRetrieve, buffer);
	}
	public override void SaveToBuffer(NetBuffer buffer) {
		base.SaveToBuffer(buffer);
		BinaryHelper.WriteToBuffer(isRetrieve, buffer);
	}
};

public class QuestItemInit : QuestItem
{
	public bool isBinding;
	public bool isRetrieve;

	public override void LoadFromBuffer(NetBuffer buffer) {
		base.LoadFromBuffer(buffer);
		BinaryHelper.ReadFromBuffer(out isBinding, buffer);
		BinaryHelper.ReadFromBuffer(out isRetrieve, buffer);
	}
	public override void SaveToBuffer(NetBuffer buffer) {
		base.SaveToBuffer(buffer);
		BinaryHelper.WriteToBuffer(isBinding, buffer);
		BinaryHelper.WriteToBuffer(isRetrieve, buffer);
	}
};

public class QuestChequeReward : QuestCheque
{
	public bool isFixed;

	public override void LoadFromBuffer(NetBuffer buffer) {
		base.LoadFromBuffer(buffer);
		BinaryHelper.ReadFromBuffer(out isFixed, buffer);
	}
	public override void SaveToBuffer(NetBuffer buffer) {
		base.SaveToBuffer(buffer);
		BinaryHelper.WriteToBuffer(isFixed, buffer);
	}
};

public class QuestItemReward : QuestItem
{
	public bool isBinding;

	public override void LoadFromBuffer(NetBuffer buffer) {
		base.LoadFromBuffer(buffer);
		BinaryHelper.ReadFromBuffer(out isBinding, buffer);
	}
	public override void SaveToBuffer(NetBuffer buffer) {
		base.SaveToBuffer(buffer);
		BinaryHelper.WriteToBuffer(isBinding, buffer);
	}
};

public enum QuestConditionType
{
	None,
	TalkNPC,
	KillCreature,
	HaveCheque,
	HaveItem,
	UseItem,
	PlayStory,
	Count,
};

public class QuestCondition : BinaryHelper.ISerializable
{
	public uint conditionType;
	public List<uint> conditionIds;
	public ulong conditionNum;
	public List<uint> conditionArgs;
	public List<bool> conditionFlags;
	public QuestObjInst forNavInfo;

	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out conditionType, buffer);
		BinaryHelper.SequenceReadFromBuffer(out conditionIds, buffer);
		BinaryHelper.ReadFromBuffer(out conditionNum, buffer);
		BinaryHelper.SequenceReadFromBuffer(out conditionArgs, buffer);
		BinaryHelper.SequenceReadFromBuffer(out conditionFlags, buffer);
		BinaryHelper.LoadFromBuffer(out forNavInfo, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(conditionType, buffer);
		BinaryHelper.SequenceWriteToBuffer(conditionIds, buffer);
		BinaryHelper.WriteToBuffer(conditionNum, buffer);
		BinaryHelper.SequenceWriteToBuffer(conditionArgs, buffer);
		BinaryHelper.SequenceWriteToBuffer(conditionFlags, buffer);
		BinaryHelper.SaveToBuffer(forNavInfo, buffer);
	}
};

public class QuestPrototype : BinaryHelper.ISerializable
{
	public class Flags : BinaryHelper.ISerializable
	{
		public bool isWatchStatus;
		public bool isAutoAccept;
		public bool isAutoSubmit;
		public bool isStoryMode;
		public bool isEnterSceneAeap;
		public bool isLeaveSceneAeap;
		public bool isCantArchive;
		public bool isCantCancel;
		public bool isAnyReqQuest;
		public bool isAnyCondition;
		public bool isRemoveFailed;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out isWatchStatus, buffer);
			BinaryHelper.ReadFromBuffer(out isAutoAccept, buffer);
			BinaryHelper.ReadFromBuffer(out isAutoSubmit, buffer);
			BinaryHelper.ReadFromBuffer(out isStoryMode, buffer);
			BinaryHelper.ReadFromBuffer(out isEnterSceneAeap, buffer);
			BinaryHelper.ReadFromBuffer(out isLeaveSceneAeap, buffer);
			BinaryHelper.ReadFromBuffer(out isCantArchive, buffer);
			BinaryHelper.ReadFromBuffer(out isCantCancel, buffer);
			BinaryHelper.ReadFromBuffer(out isAnyReqQuest, buffer);
			BinaryHelper.ReadFromBuffer(out isAnyCondition, buffer);
			BinaryHelper.ReadFromBuffer(out isRemoveFailed, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(isWatchStatus, buffer);
			BinaryHelper.WriteToBuffer(isAutoAccept, buffer);
			BinaryHelper.WriteToBuffer(isAutoSubmit, buffer);
			BinaryHelper.WriteToBuffer(isStoryMode, buffer);
			BinaryHelper.WriteToBuffer(isEnterSceneAeap, buffer);
			BinaryHelper.WriteToBuffer(isLeaveSceneAeap, buffer);
			BinaryHelper.WriteToBuffer(isCantArchive, buffer);
			BinaryHelper.WriteToBuffer(isCantCancel, buffer);
			BinaryHelper.WriteToBuffer(isAnyReqQuest, buffer);
			BinaryHelper.WriteToBuffer(isAnyCondition, buffer);
			BinaryHelper.WriteToBuffer(isRemoveFailed, buffer);
		}
	};

	public uint questTypeID;
	public uint questClass;
	public Flags questFlags;
	public QuestObjInst questPublisher;
	public QuestObjInst questVerifier;
	public QuestObjInst questGuider;
	public uint questTimeMax;

	public uint questRepeatType;
	public uint questRepeatMax;

	public uint questReqMinLv;
	public uint questReqMaxLv;
	public uint questReqCareer;
	public uint questReqGender;
	public List<uint> questReqQuests;
	public List<QuestChequeReq> questReqCheques;
	public List<QuestItemReq> questReqItems;
	public QuestQuestionReq questReqQuestion;
	public QuestScript questReqExtra;

	public List<QuestChequeInit> questInitCheques;
	public List<QuestItemInit> questInitItems;
	public QuestScript questInitExtra;

	public List<QuestChequeReward> questRewardCheques;
	public List<QuestItemReward> questRewardItems;
	public QuestScript questRewardExtra;

	public List<QuestScript> questScripts;
	public List<QuestCondition> questConditions;

	public static string GetTableName() {
		return "quest_prototype";
	}
	public static string GetTableKeyName() {
		return "questTypeID";
	}
	public uint GetTableKeyValue() {
		return (uint)questTypeID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out questTypeID, buffer);
		BinaryHelper.ReadFromBuffer(out questClass, buffer);
		BinaryHelper.LoadFromBuffer(out questFlags, buffer);
		BinaryHelper.LoadFromBuffer(out questPublisher, buffer);
		BinaryHelper.LoadFromBuffer(out questVerifier, buffer);
		BinaryHelper.LoadFromBuffer(out questGuider, buffer);
		BinaryHelper.ReadFromBuffer(out questTimeMax, buffer);
		BinaryHelper.ReadFromBuffer(out questRepeatType, buffer);
		BinaryHelper.ReadFromBuffer(out questRepeatMax, buffer);
		BinaryHelper.ReadFromBuffer(out questReqMinLv, buffer);
		BinaryHelper.ReadFromBuffer(out questReqMaxLv, buffer);
		BinaryHelper.ReadFromBuffer(out questReqCareer, buffer);
		BinaryHelper.ReadFromBuffer(out questReqGender, buffer);
		BinaryHelper.SequenceReadFromBuffer(out questReqQuests, buffer);
		BinaryHelper.BlockSequenceReadFromBuffer(out questReqCheques, buffer);
		BinaryHelper.BlockSequenceReadFromBuffer(out questReqItems, buffer);
		BinaryHelper.LoadFromBuffer(out questReqQuestion, buffer);
		BinaryHelper.LoadFromBuffer(out questReqExtra, buffer);
		BinaryHelper.BlockSequenceReadFromBuffer(out questInitCheques, buffer);
		BinaryHelper.BlockSequenceReadFromBuffer(out questInitItems, buffer);
		BinaryHelper.LoadFromBuffer(out questInitExtra, buffer);
		BinaryHelper.BlockSequenceReadFromBuffer(out questRewardCheques, buffer);
		BinaryHelper.BlockSequenceReadFromBuffer(out questRewardItems, buffer);
		BinaryHelper.LoadFromBuffer(out questRewardExtra, buffer);
		BinaryHelper.BlockSequenceReadFromBuffer(out questScripts, buffer);
		BinaryHelper.BlockSequenceReadFromBuffer(out questConditions, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(questTypeID, buffer);
		BinaryHelper.WriteToBuffer(questClass, buffer);
		BinaryHelper.SaveToBuffer(questFlags, buffer);
		BinaryHelper.SaveToBuffer(questPublisher, buffer);
		BinaryHelper.SaveToBuffer(questVerifier, buffer);
		BinaryHelper.SaveToBuffer(questGuider, buffer);
		BinaryHelper.WriteToBuffer(questTimeMax, buffer);
		BinaryHelper.WriteToBuffer(questRepeatType, buffer);
		BinaryHelper.WriteToBuffer(questRepeatMax, buffer);
		BinaryHelper.WriteToBuffer(questReqMinLv, buffer);
		BinaryHelper.WriteToBuffer(questReqMaxLv, buffer);
		BinaryHelper.WriteToBuffer(questReqCareer, buffer);
		BinaryHelper.WriteToBuffer(questReqGender, buffer);
		BinaryHelper.SequenceWriteToBuffer(questReqQuests, buffer);
		BinaryHelper.BlockSequenceWriteToBuffer(questReqCheques, buffer);
		BinaryHelper.BlockSequenceWriteToBuffer(questReqItems, buffer);
		BinaryHelper.SaveToBuffer(questReqQuestion, buffer);
		BinaryHelper.SaveToBuffer(questReqExtra, buffer);
		BinaryHelper.BlockSequenceWriteToBuffer(questInitCheques, buffer);
		BinaryHelper.BlockSequenceWriteToBuffer(questInitItems, buffer);
		BinaryHelper.SaveToBuffer(questInitExtra, buffer);
		BinaryHelper.BlockSequenceWriteToBuffer(questRewardCheques, buffer);
		BinaryHelper.BlockSequenceWriteToBuffer(questRewardItems, buffer);
		BinaryHelper.SaveToBuffer(questRewardExtra, buffer);
		BinaryHelper.BlockSequenceWriteToBuffer(questScripts, buffer);
		BinaryHelper.BlockSequenceWriteToBuffer(questConditions, buffer);
	}
};


public class QuestCreatureVisible : BinaryHelper.ISerializable
{
	public uint questTypeID;
	public sbyte questWhenType;
	public bool isVisible;
	public List<uint> spawnIDs;

	public static string GetTableName() {
		return "quest_creature_visible";
	}
	public static string GetTableKeyName() {
		return string.Empty;
	}
	public uint GetTableKeyValue() {
		return (uint)0;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out questTypeID, buffer);
		BinaryHelper.ReadFromBuffer(out questWhenType, buffer);
		BinaryHelper.ReadFromBuffer(out isVisible, buffer);
		BinaryHelper.SequenceReadFromBuffer(out spawnIDs, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(questTypeID, buffer);
		BinaryHelper.WriteToBuffer(questWhenType, buffer);
		BinaryHelper.WriteToBuffer(isVisible, buffer);
		BinaryHelper.SequenceWriteToBuffer(spawnIDs, buffer);
	}
};
