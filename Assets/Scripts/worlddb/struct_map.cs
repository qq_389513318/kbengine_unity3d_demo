using System.Collections.Generic;

public class MapInfo : BinaryHelper.ISerializable
{
	public enum Type
	{
		VoidSpace,
		WorldMap,
		Dungeon,
		Story,
		Count,
	};
	public class Flags : BinaryHelper.ISerializable
	{
		public bool isParallelMap;
		public bool isRecoveryHPDisable;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out isParallelMap, buffer);
			BinaryHelper.ReadFromBuffer(out isRecoveryHPDisable, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(isParallelMap, buffer);
			BinaryHelper.WriteToBuffer(isRecoveryHPDisable, buffer);
		}
	};
	public uint Id;
	public uint type;
	public Flags flags;
	public string strName;
	public string strSceneFile;
	public float viewing_distance;
	public uint load_value;
	public uint pop_map_id;
	public float pop_pos_x;
	public float pop_pos_y;
	public float pop_pos_z;
	public float pop_o;

	public static string GetTableName() {
		return "map_info";
	}
	public static string GetTableKeyName() {
		return "Id";
	}
	public uint GetTableKeyValue() {
		return (uint)Id;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out Id, buffer);
		BinaryHelper.ReadFromBuffer(out type, buffer);
		BinaryHelper.LoadFromBuffer(out flags, buffer);
		BinaryHelper.ReadFromBuffer(out strName, buffer);
		BinaryHelper.ReadFromBuffer(out strSceneFile, buffer);
		BinaryHelper.ReadFromBuffer(out viewing_distance, buffer);
		BinaryHelper.ReadFromBuffer(out load_value, buffer);
		BinaryHelper.ReadFromBuffer(out pop_map_id, buffer);
		BinaryHelper.ReadFromBuffer(out pop_pos_x, buffer);
		BinaryHelper.ReadFromBuffer(out pop_pos_y, buffer);
		BinaryHelper.ReadFromBuffer(out pop_pos_z, buffer);
		BinaryHelper.ReadFromBuffer(out pop_o, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(Id, buffer);
		BinaryHelper.WriteToBuffer(type, buffer);
		BinaryHelper.SaveToBuffer(flags, buffer);
		BinaryHelper.WriteToBuffer(strName, buffer);
		BinaryHelper.WriteToBuffer(strSceneFile, buffer);
		BinaryHelper.WriteToBuffer(viewing_distance, buffer);
		BinaryHelper.WriteToBuffer(load_value, buffer);
		BinaryHelper.WriteToBuffer(pop_map_id, buffer);
		BinaryHelper.WriteToBuffer(pop_pos_x, buffer);
		BinaryHelper.WriteToBuffer(pop_pos_y, buffer);
		BinaryHelper.WriteToBuffer(pop_pos_z, buffer);
		BinaryHelper.WriteToBuffer(pop_o, buffer);
	}
};

public class MapZone : BinaryHelper.ISerializable
{
	public uint Id;
	public uint parentId;
	public uint priority;
	public uint map_id;
	public uint map_type;
	public float x1, y1, z1;
	public float x2, y2, z2;
	public uint pvp_flags;

	public static string GetTableName() {
		return "map_zone";
	}
	public static string GetTableKeyName() {
		return "Id";
	}
	public uint GetTableKeyValue() {
		return (uint)Id;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out Id, buffer);
		BinaryHelper.ReadFromBuffer(out parentId, buffer);
		BinaryHelper.ReadFromBuffer(out priority, buffer);
		BinaryHelper.ReadFromBuffer(out map_id, buffer);
		BinaryHelper.ReadFromBuffer(out map_type, buffer);
		BinaryHelper.ReadFromBuffer(out x1, buffer);
		BinaryHelper.ReadFromBuffer(out y1, buffer);
		BinaryHelper.ReadFromBuffer(out z1, buffer);
		BinaryHelper.ReadFromBuffer(out x2, buffer);
		BinaryHelper.ReadFromBuffer(out y2, buffer);
		BinaryHelper.ReadFromBuffer(out z2, buffer);
		BinaryHelper.ReadFromBuffer(out pvp_flags, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(Id, buffer);
		BinaryHelper.WriteToBuffer(parentId, buffer);
		BinaryHelper.WriteToBuffer(priority, buffer);
		BinaryHelper.WriteToBuffer(map_id, buffer);
		BinaryHelper.WriteToBuffer(map_type, buffer);
		BinaryHelper.WriteToBuffer(x1, buffer);
		BinaryHelper.WriteToBuffer(y1, buffer);
		BinaryHelper.WriteToBuffer(z1, buffer);
		BinaryHelper.WriteToBuffer(x2, buffer);
		BinaryHelper.WriteToBuffer(y2, buffer);
		BinaryHelper.WriteToBuffer(z2, buffer);
		BinaryHelper.WriteToBuffer(pvp_flags, buffer);
	}
};

public class MapGraveyard : BinaryHelper.ISerializable
{
	public uint Id;
	public uint map_id;
	public uint map_type;
	public float x, y, z, o;

	public static string GetTableName() {
		return "map_graveyard";
	}
	public static string GetTableKeyName() {
		return "Id";
	}
	public uint GetTableKeyValue() {
		return (uint)Id;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out Id, buffer);
		BinaryHelper.ReadFromBuffer(out map_id, buffer);
		BinaryHelper.ReadFromBuffer(out map_type, buffer);
		BinaryHelper.ReadFromBuffer(out x, buffer);
		BinaryHelper.ReadFromBuffer(out y, buffer);
		BinaryHelper.ReadFromBuffer(out z, buffer);
		BinaryHelper.ReadFromBuffer(out o, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(Id, buffer);
		BinaryHelper.WriteToBuffer(map_id, buffer);
		BinaryHelper.WriteToBuffer(map_type, buffer);
		BinaryHelper.WriteToBuffer(x, buffer);
		BinaryHelper.WriteToBuffer(y, buffer);
		BinaryHelper.WriteToBuffer(z, buffer);
		BinaryHelper.WriteToBuffer(o, buffer);
	}
};

public class TeleportPoint : BinaryHelper.ISerializable
{
	public uint Id;
	public string name;
	public uint map_id;
	public uint map_type;
	public float x, y, z, o;
	public uint trait;

	public static string GetTableName() {
		return "teleport_point";
	}
	public static string GetTableKeyName() {
		return "Id";
	}
	public uint GetTableKeyValue() {
		return (uint)Id;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out Id, buffer);
		BinaryHelper.ReadFromBuffer(out name, buffer);
		BinaryHelper.ReadFromBuffer(out map_id, buffer);
		BinaryHelper.ReadFromBuffer(out map_type, buffer);
		BinaryHelper.ReadFromBuffer(out x, buffer);
		BinaryHelper.ReadFromBuffer(out y, buffer);
		BinaryHelper.ReadFromBuffer(out z, buffer);
		BinaryHelper.ReadFromBuffer(out o, buffer);
		BinaryHelper.ReadFromBuffer(out trait, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(Id, buffer);
		BinaryHelper.WriteToBuffer(name, buffer);
		BinaryHelper.WriteToBuffer(map_id, buffer);
		BinaryHelper.WriteToBuffer(map_type, buffer);
		BinaryHelper.WriteToBuffer(x, buffer);
		BinaryHelper.WriteToBuffer(y, buffer);
		BinaryHelper.WriteToBuffer(z, buffer);
		BinaryHelper.WriteToBuffer(o, buffer);
		BinaryHelper.WriteToBuffer(trait, buffer);
	}
};

public class WayPoint : BinaryHelper.ISerializable
{
	public uint Id;
	public uint first_wp_id;
	public uint prev_wp_id;
	public uint next_wp_id;
	public uint map_id;
	public float x, y, z;
	public int keep_idle;
	public uint emote_state;

	public static string GetTableName() {
		return "way_point";
	}
	public static string GetTableKeyName() {
		return "Id";
	}
	public uint GetTableKeyValue() {
		return (uint)Id;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out Id, buffer);
		BinaryHelper.ReadFromBuffer(out first_wp_id, buffer);
		BinaryHelper.ReadFromBuffer(out prev_wp_id, buffer);
		BinaryHelper.ReadFromBuffer(out next_wp_id, buffer);
		BinaryHelper.ReadFromBuffer(out map_id, buffer);
		BinaryHelper.ReadFromBuffer(out x, buffer);
		BinaryHelper.ReadFromBuffer(out y, buffer);
		BinaryHelper.ReadFromBuffer(out z, buffer);
		BinaryHelper.ReadFromBuffer(out keep_idle, buffer);
		BinaryHelper.ReadFromBuffer(out emote_state, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(Id, buffer);
		BinaryHelper.WriteToBuffer(first_wp_id, buffer);
		BinaryHelper.WriteToBuffer(prev_wp_id, buffer);
		BinaryHelper.WriteToBuffer(next_wp_id, buffer);
		BinaryHelper.WriteToBuffer(map_id, buffer);
		BinaryHelper.WriteToBuffer(x, buffer);
		BinaryHelper.WriteToBuffer(y, buffer);
		BinaryHelper.WriteToBuffer(z, buffer);
		BinaryHelper.WriteToBuffer(keep_idle, buffer);
		BinaryHelper.WriteToBuffer(emote_state, buffer);
	}
};

public class LandmarkPoint : BinaryHelper.ISerializable
{
	public uint Id;
	public string name;
	public uint map_id;
	public uint map_type;
	public float x, y, z;

	public static string GetTableName() {
		return "landmark_point";
	}
	public static string GetTableKeyName() {
		return "Id";
	}
	public uint GetTableKeyValue() {
		return (uint)Id;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out Id, buffer);
		BinaryHelper.ReadFromBuffer(out name, buffer);
		BinaryHelper.ReadFromBuffer(out map_id, buffer);
		BinaryHelper.ReadFromBuffer(out map_type, buffer);
		BinaryHelper.ReadFromBuffer(out x, buffer);
		BinaryHelper.ReadFromBuffer(out y, buffer);
		BinaryHelper.ReadFromBuffer(out z, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(Id, buffer);
		BinaryHelper.WriteToBuffer(name, buffer);
		BinaryHelper.WriteToBuffer(map_id, buffer);
		BinaryHelper.WriteToBuffer(map_type, buffer);
		BinaryHelper.WriteToBuffer(x, buffer);
		BinaryHelper.WriteToBuffer(y, buffer);
		BinaryHelper.WriteToBuffer(z, buffer);
	}
};
