using System.Collections.Generic;

public enum SpellStageType
{
	Chant,
	Channel,
	Cleanup,
};

public enum SpellResumeType
{
	RemoveWhenOffline,
	ContinueWhenOnline,
	ContinueByTime,
};

public enum SpellEffectStyle
{
	None,
	Positive,
	Negative,
	All,
};

public enum SpellInterruptBy
{
	None,
	Move,
	Injured,
};

public enum SpellEffectType
{
	None = 0,
	Attack,
	AuraAttack,
	ChangeProp,
	ChangeSpeed,
	Invincible,
	Invisible,
	Fetter,
	Stun,

	AuraObject = 300,

	Teleport = 500,
	Mine,
};

public enum SpellTargetType
{
	Any,
	Friend,
	Enemy,
	TeamMember,
	Count,
};

public enum SpellSelectType
{
	None,
	Self,
	Friend,
	Enemy,
	Friends,
	Enemies,
	SelfOrFriend,
	FocusFriends,
	FocusEnemies,
	TeamMember,
	TeamMembers,
	SelfOrTeamMember,
	FocusTeamMembers,
	Reference,
	Count,
};

public enum SpellSelectMode
{
	Circle,
	Sector,
	Rect,
	Count,
};

public enum SpellPassiveMode
{
	Status,
	Event,
	Count,
};

public enum SpellPassiveBy
{
	StatusNone = 0,
	StatusHPValue,
	StatusHPRate,
	StatusMax,

	EventHit = 0,
	EventHitBy,
	EventMax,
};

public class SpellInfo : BinaryHelper.ISerializable
{
	public class Flags : BinaryHelper.ISerializable
	{
		public bool isExclusive;
		public bool isPassive;
		public bool isAppearance;
		public bool isSync2Client;
		public bool isSync2AllClient;
		public bool isTargetDeadable;
		public bool isCheckCastDist;
		public bool isIgnoreOutOfControl;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out isExclusive, buffer);
			BinaryHelper.ReadFromBuffer(out isPassive, buffer);
			BinaryHelper.ReadFromBuffer(out isAppearance, buffer);
			BinaryHelper.ReadFromBuffer(out isSync2Client, buffer);
			BinaryHelper.ReadFromBuffer(out isSync2AllClient, buffer);
			BinaryHelper.ReadFromBuffer(out isTargetDeadable, buffer);
			BinaryHelper.ReadFromBuffer(out isCheckCastDist, buffer);
			BinaryHelper.ReadFromBuffer(out isIgnoreOutOfControl, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(isExclusive, buffer);
			BinaryHelper.WriteToBuffer(isPassive, buffer);
			BinaryHelper.WriteToBuffer(isAppearance, buffer);
			BinaryHelper.WriteToBuffer(isSync2Client, buffer);
			BinaryHelper.WriteToBuffer(isSync2AllClient, buffer);
			BinaryHelper.WriteToBuffer(isTargetDeadable, buffer);
			BinaryHelper.WriteToBuffer(isCheckCastDist, buffer);
			BinaryHelper.WriteToBuffer(isIgnoreOutOfControl, buffer);
		}
	};
	public uint spellID;
	public string spellIcon;
	public Flags spellFlags;
	public byte spellBuffType;
	public byte spellCooldownType;
	public byte spellTargetType;
	public byte spellPassiveMode;
	public byte spellPassiveBy;
	public List<float> spellPassiveArgs;
	public float spellPassiveChance;
	public uint spellLimitCareer;
	public uint spellLimitMapType;
	public uint spellLimitMapID;
	public uint spellLimitScriptID;
	public string spellLimitScriptArgs;
	public uint spellInterruptBys;
	public List<uint> spellStageTime;

	public static string GetTableName() {
		return "spell_info";
	}
	public static string GetTableKeyName() {
		return "spellID";
	}
	public uint GetTableKeyValue() {
		return (uint)spellID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out spellID, buffer);
		BinaryHelper.ReadFromBuffer(out spellIcon, buffer);
		BinaryHelper.LoadFromBuffer(out spellFlags, buffer);
		BinaryHelper.ReadFromBuffer(out spellBuffType, buffer);
		BinaryHelper.ReadFromBuffer(out spellCooldownType, buffer);
		BinaryHelper.ReadFromBuffer(out spellTargetType, buffer);
		BinaryHelper.ReadFromBuffer(out spellPassiveMode, buffer);
		BinaryHelper.ReadFromBuffer(out spellPassiveBy, buffer);
		BinaryHelper.SequenceReadFromBuffer(out spellPassiveArgs, buffer);
		BinaryHelper.ReadFromBuffer(out spellPassiveChance, buffer);
		BinaryHelper.ReadFromBuffer(out spellLimitCareer, buffer);
		BinaryHelper.ReadFromBuffer(out spellLimitMapType, buffer);
		BinaryHelper.ReadFromBuffer(out spellLimitMapID, buffer);
		BinaryHelper.ReadFromBuffer(out spellLimitScriptID, buffer);
		BinaryHelper.ReadFromBuffer(out spellLimitScriptArgs, buffer);
		BinaryHelper.ReadFromBuffer(out spellInterruptBys, buffer);
		BinaryHelper.SequenceReadFromBuffer(out spellStageTime, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(spellID, buffer);
		BinaryHelper.WriteToBuffer(spellIcon, buffer);
		BinaryHelper.SaveToBuffer(spellFlags, buffer);
		BinaryHelper.WriteToBuffer(spellBuffType, buffer);
		BinaryHelper.WriteToBuffer(spellCooldownType, buffer);
		BinaryHelper.WriteToBuffer(spellTargetType, buffer);
		BinaryHelper.WriteToBuffer(spellPassiveMode, buffer);
		BinaryHelper.WriteToBuffer(spellPassiveBy, buffer);
		BinaryHelper.SequenceWriteToBuffer(spellPassiveArgs, buffer);
		BinaryHelper.WriteToBuffer(spellPassiveChance, buffer);
		BinaryHelper.WriteToBuffer(spellLimitCareer, buffer);
		BinaryHelper.WriteToBuffer(spellLimitMapType, buffer);
		BinaryHelper.WriteToBuffer(spellLimitMapID, buffer);
		BinaryHelper.WriteToBuffer(spellLimitScriptID, buffer);
		BinaryHelper.WriteToBuffer(spellLimitScriptArgs, buffer);
		BinaryHelper.WriteToBuffer(spellInterruptBys, buffer);
		BinaryHelper.SequenceWriteToBuffer(spellStageTime, buffer);
	}
};

public class SpellLevelInfo : BinaryHelper.ISerializable
{
	public uint spellLevelID;
	public uint spellID;
	public uint spellLimitLevel;
	public float spellCastDistMin;
	public float spellCastDistMax;
	public uint spellEvalScore;
	public uint spellCostHP;
	public float spellCostHPRate;
	public uint spellCostMP;
	public float spellCostMPRate;
	public uint spellCDTime;

	public static string GetTableName() {
		return "spell_level_info";
	}
	public static string GetTableKeyName() {
		return "spellLevelID";
	}
	public uint GetTableKeyValue() {
		return (uint)spellLevelID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out spellLevelID, buffer);
		BinaryHelper.ReadFromBuffer(out spellID, buffer);
		BinaryHelper.ReadFromBuffer(out spellLimitLevel, buffer);
		BinaryHelper.ReadFromBuffer(out spellCastDistMin, buffer);
		BinaryHelper.ReadFromBuffer(out spellCastDistMax, buffer);
		BinaryHelper.ReadFromBuffer(out spellEvalScore, buffer);
		BinaryHelper.ReadFromBuffer(out spellCostHP, buffer);
		BinaryHelper.ReadFromBuffer(out spellCostHPRate, buffer);
		BinaryHelper.ReadFromBuffer(out spellCostMP, buffer);
		BinaryHelper.ReadFromBuffer(out spellCostMPRate, buffer);
		BinaryHelper.ReadFromBuffer(out spellCDTime, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(spellLevelID, buffer);
		BinaryHelper.WriteToBuffer(spellID, buffer);
		BinaryHelper.WriteToBuffer(spellLimitLevel, buffer);
		BinaryHelper.WriteToBuffer(spellCastDistMin, buffer);
		BinaryHelper.WriteToBuffer(spellCastDistMax, buffer);
		BinaryHelper.WriteToBuffer(spellEvalScore, buffer);
		BinaryHelper.WriteToBuffer(spellCostHP, buffer);
		BinaryHelper.WriteToBuffer(spellCostHPRate, buffer);
		BinaryHelper.WriteToBuffer(spellCostMP, buffer);
		BinaryHelper.WriteToBuffer(spellCostMPRate, buffer);
		BinaryHelper.WriteToBuffer(spellCDTime, buffer);
	}
};

public class SpellLevelEffectInfo : BinaryHelper.ISerializable
{
	public class Flags : BinaryHelper.ISerializable
	{
		public bool isResumable;
		public bool isDeadResumable;
		public bool isSync2Client;
		public bool isSync2AllClient;
		public bool isTargetDeadable;
		public bool isClientSelectTarget;

		public virtual void LoadFromBuffer(NetBuffer buffer) {
			BinaryHelper.ReadFromBuffer(out isResumable, buffer);
			BinaryHelper.ReadFromBuffer(out isDeadResumable, buffer);
			BinaryHelper.ReadFromBuffer(out isSync2Client, buffer);
			BinaryHelper.ReadFromBuffer(out isSync2AllClient, buffer);
			BinaryHelper.ReadFromBuffer(out isTargetDeadable, buffer);
			BinaryHelper.ReadFromBuffer(out isClientSelectTarget, buffer);
		}
		public virtual void SaveToBuffer(NetBuffer buffer) {
			BinaryHelper.WriteToBuffer(isResumable, buffer);
			BinaryHelper.WriteToBuffer(isDeadResumable, buffer);
			BinaryHelper.WriteToBuffer(isSync2Client, buffer);
			BinaryHelper.WriteToBuffer(isSync2AllClient, buffer);
			BinaryHelper.WriteToBuffer(isTargetDeadable, buffer);
			BinaryHelper.WriteToBuffer(isClientSelectTarget, buffer);
		}
	};
	public uint spellLevelEffectID;
	public uint spellID;
	public uint spellLevelID;
	public Flags spellEffectFlags;
	public byte spellStageTrigger;
	public uint spellDelayTrigger;
	public byte spellSelectType;
	public byte spellSelectMode;
	public List<float> spellSelectArgs;
	public uint spellSelectNumber;
	public uint spellDelayEffective;
	public float spellEffectiveChance;
	public uint spellEffectStyle;
	public uint spellEffectType;
	public string spellEffectArgs;

	public static string GetTableName() {
		return "spell_level_effect_info";
	}
	public static string GetTableKeyName() {
		return "spellLevelEffectID";
	}
	public uint GetTableKeyValue() {
		return (uint)spellLevelEffectID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out spellLevelEffectID, buffer);
		BinaryHelper.ReadFromBuffer(out spellID, buffer);
		BinaryHelper.ReadFromBuffer(out spellLevelID, buffer);
		BinaryHelper.LoadFromBuffer(out spellEffectFlags, buffer);
		BinaryHelper.ReadFromBuffer(out spellStageTrigger, buffer);
		BinaryHelper.ReadFromBuffer(out spellDelayTrigger, buffer);
		BinaryHelper.ReadFromBuffer(out spellSelectType, buffer);
		BinaryHelper.ReadFromBuffer(out spellSelectMode, buffer);
		BinaryHelper.SequenceReadFromBuffer(out spellSelectArgs, buffer);
		BinaryHelper.ReadFromBuffer(out spellSelectNumber, buffer);
		BinaryHelper.ReadFromBuffer(out spellDelayEffective, buffer);
		BinaryHelper.ReadFromBuffer(out spellEffectiveChance, buffer);
		BinaryHelper.ReadFromBuffer(out spellEffectStyle, buffer);
		BinaryHelper.ReadFromBuffer(out spellEffectType, buffer);
		BinaryHelper.ReadFromBuffer(out spellEffectArgs, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(spellLevelEffectID, buffer);
		BinaryHelper.WriteToBuffer(spellID, buffer);
		BinaryHelper.WriteToBuffer(spellLevelID, buffer);
		BinaryHelper.SaveToBuffer(spellEffectFlags, buffer);
		BinaryHelper.WriteToBuffer(spellStageTrigger, buffer);
		BinaryHelper.WriteToBuffer(spellDelayTrigger, buffer);
		BinaryHelper.WriteToBuffer(spellSelectType, buffer);
		BinaryHelper.WriteToBuffer(spellSelectMode, buffer);
		BinaryHelper.SequenceWriteToBuffer(spellSelectArgs, buffer);
		BinaryHelper.WriteToBuffer(spellSelectNumber, buffer);
		BinaryHelper.WriteToBuffer(spellDelayEffective, buffer);
		BinaryHelper.WriteToBuffer(spellEffectiveChance, buffer);
		BinaryHelper.WriteToBuffer(spellEffectStyle, buffer);
		BinaryHelper.WriteToBuffer(spellEffectType, buffer);
		BinaryHelper.WriteToBuffer(spellEffectArgs, buffer);
	}
};

public enum AuraSelectType
{
	Friend,
	Enemy,
	Player,
	Creature,
	TeamMember,
	Count,
};
