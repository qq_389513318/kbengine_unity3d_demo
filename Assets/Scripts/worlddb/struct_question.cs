using System.Collections.Generic;

public class QuestQuestion : BinaryHelper.ISerializable
{
	public uint Id;

	public static string GetTableName() {
		return "QuestQuestion";
	}
	public static string GetTableKeyName() {
		return string.Empty;
	}
	public uint GetTableKeyValue() {
		return (uint)0;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out Id, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(Id, buffer);
	}
};
