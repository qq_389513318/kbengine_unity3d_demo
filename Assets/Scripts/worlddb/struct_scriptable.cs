using System.Collections.Generic;

public enum ScriptType
{
	None = 0,
	AIGraphml = 1,
	CharSpawn,
	CharDead,
	CharPlay,
	SObjSpawn = 11,
	SObjPlay,
	QuestReq = 21,
	QuestInit,
	QuestReward,
	QuestEvent,
	CanCastSpell = 31,
	UseItem = 41,
};

public class Scriptable : BinaryHelper.ISerializable
{
	public uint scriptId;
	public uint scriptType;
	public string scriptFile;

	public static string GetTableName() {
		return "scriptable";
	}
	public static string GetTableKeyName() {
		return "scriptId";
	}
	public uint GetTableKeyValue() {
		return (uint)scriptId;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out scriptId, buffer);
		BinaryHelper.ReadFromBuffer(out scriptType, buffer);
		BinaryHelper.ReadFromBuffer(out scriptFile, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(scriptId, buffer);
		BinaryHelper.WriteToBuffer(scriptType, buffer);
		BinaryHelper.WriteToBuffer(scriptFile, buffer);
	}
};
