using System;
using System.Collections.Generic;

public class QuestStorage
{
    private Dictionary<UInt64, QuestLog> m_allQuestLog;

    public QuestLog GetQuest(UInt64 questGuid)
    {
        return m_allQuestLog.TryGetValue(questGuid, out QuestLog pQuestLog) ? pQuestLog : null;
    }

    public void AddQuest(QuestLog quest)
    {
        m_allQuestLog.Add(quest.GetQuestProp().questGuid, quest);
    }

    public void RemoveQuest(UInt64 questGuid)
    {
        m_allQuestLog.Remove(questGuid);
    }

    public void LoadFromCreatePacket(NetPacket pck)
    {
        pck.Read(out UInt16 questNum);
        m_allQuestLog = new Dictionary<UInt64, QuestLog>(questNum);
        for (UInt16 i = 0; i < questNum; ++i)
        {
            var pQuestLog = new QuestLog();
            pQuestLog.LoadFromCreatePacket(pck);
            AddQuest(pQuestLog);
            UI.Instance.PushPlayMsg(string.Format("��������`{0}`.", pQuestLog.GetName()));
        }
    }
}
