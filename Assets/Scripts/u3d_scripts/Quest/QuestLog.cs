using System;
using System.Collections.Generic;

public class inst_quest_prop
{
    public enum Flag
    {
        IsFinished,
        IsFailed,
    };
    public struct Condition
    {
        public Int64 progress;
        public Int64 userData;
    };

    public UInt32 questGuid;
    public UInt32 questTypeID;
    public UInt32 questFlags;
    public Int64 questExpireTime;
    public List<Condition> questConditions;

    public void Load(NetBuffer pck)
    {
        pck.Read(out questGuid);
        pck.Read(out questTypeID);
        pck.Read(out questFlags);
        pck.Read(out questExpireTime);
        pck.Read(out byte questNum);
        questConditions = new List<Condition>(questNum);
        for (byte i = 0; i < questNum; ++i)
        {
            Condition condition;
            pck.Read(out condition.progress);
            pck.Read(out condition.userData);
            questConditions.Add(condition);
        }
    }
}

public class QuestLog
{
    private QuestPrototype m_pQuestProto = null;
    private inst_quest_prop m_questProp = new inst_quest_prop();

    public QuestPrototype GetQuestProto()
    {
        return m_pQuestProto;
    }
    public inst_quest_prop GetQuestProp()
    {
        return m_questProp;
    }

    public string GetName()
    {
        return DBMgr.Instance.GetText(
            m_questProp.questTypeID, STRING_TEXT_TYPE.QUEST_NAME);
    }

    public void LoadFromCreatePacket(NetPacket pck)
    {
        m_questProp.Load(pck);
        m_pQuestProto = DBMgr.Instance.
            tblQuestPrototype.GetEntry(m_questProp.questTypeID);
    }
}
