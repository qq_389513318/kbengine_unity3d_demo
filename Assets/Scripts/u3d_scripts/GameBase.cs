using System;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Networking;

[Serializable]
public class GameCharacterInfo
{
    public UInt32 ServerID;
    public UInt32 CharacterID;
    public string CharacterName;
    public UInt32 CharacterLevel;
}

[Serializable]
public class GameServerInfo
{
    public UInt32 ID;
    public string ExternalIP;
    public UInt16 ExternalPort;
    public string InternalIP;
    public string InternalName;
    public UInt32 LogicId;
    public string LogicName;
    public byte LogicOpenStatus;
    public UInt32 LogicSpecialFlags;
    public bool IsOnline;
}

public class HTTPRespBase
{
    public int error;
    public string message;
}

public class AcceptAllCertificates : CertificateHandler
{
    protected override bool ValidateCertificate(byte[] certificateData)
    {
        return true;
    }
}

[StructLayout(LayoutKind.Explicit)]
public struct ObjGUID
{
    [FieldOffset(0)]
    public UInt64 objGUID;

    [FieldOffset(0)]
    public UInt32 UID;
    [FieldOffset(4)]
    public UInt16 TID;
    [FieldOffset(6)]
    public UInt16 SID;

    static public bool operator==(ObjGUID arg1, ObjGUID arg2)
    {
        return arg1.objGUID == arg2.objGUID;
    }
    static public bool operator !=(ObjGUID arg1, ObjGUID arg2)
    {
        return arg1.objGUID != arg2.objGUID;
    }
    public override bool Equals(object other)
    {
        return other is ObjGUID && (ObjGUID)other == this;
    }
    public override int GetHashCode()
    {
        return objGUID.GetHashCode();
    }
}

[StructLayout(LayoutKind.Explicit)]
public struct InstGUID
{
    [FieldOffset(0)]
    public UInt64 instGUID;

    [FieldOffset(0)]
    public UInt32 UID;
    [FieldOffset(4)]
    public UInt16 MAPID;
    [FieldOffset(6)]
    public UInt16 TID;

    static public bool operator ==(InstGUID arg1, InstGUID arg2)
    {
        return arg1.instGUID == arg2.instGUID;
    }
    static public bool operator !=(InstGUID arg1, InstGUID arg2)
    {
        return arg1.instGUID != arg2.instGUID;
    }
    public override bool Equals(object other)
    {
        return other is InstGUID && (InstGUID)other == this;
    }
    public override int GetHashCode()
    {
        return instGUID.GetHashCode();
    }
}

public struct NetPacketHelper
{
    private readonly NetPacket m_pck;
    public NetPacketHelper(NetPacket pck)
    {
        m_pck = pck;
    }
    public void Read(out ObjGUID arg)
    {
        arg = default;
        m_pck.Read(out arg.objGUID);
    }
    public void Read(out InstGUID arg)
    {
        arg = default;
        m_pck.Read(out arg.instGUID);
    }
    public void Read(out Vector3 arg)
    {
        arg = default;
        m_pck.Read(out arg.x);
        m_pck.Read(out arg.y);
        m_pck.Read(out arg.z);
    }
    public void Write(ObjGUID arg)
    {
        m_pck.Write(arg.objGUID);
    }
    public void Write(InstGUID arg)
    {
        m_pck.Write(arg.instGUID);
    }
    public void Write(Vector3 arg)
    {
        m_pck.Write(arg.x);
        m_pck.Write(arg.y);
        m_pck.Write(arg.z);
    }
}
