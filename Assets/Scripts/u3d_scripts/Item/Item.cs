using System;

public class inst_item_prop
{
    public UInt32 itemGuid;
    public UInt32 itemTypeID;
    public UInt32 itemQuestID;
    public UInt32 itemCount;
    public UInt32 itemOwner;
    public UInt32 itemFlags;

    public void Load(NetBuffer pck)
    {
        pck.Read(out itemGuid);
        pck.Read(out itemTypeID);
        pck.Read(out itemQuestID);
        pck.Read(out itemCount);
        pck.Read(out itemOwner);
        pck.Read(out itemFlags);
    }
}

public class Item
{
    private UInt32 m_slot;
    private inst_item_prop m_itemProp = new inst_item_prop();

    public void LoadFromCreatePacket(UInt32 slot, NetPacket pck)
    {
        m_slot = slot;
        m_itemProp.Load(pck);
    }
}
