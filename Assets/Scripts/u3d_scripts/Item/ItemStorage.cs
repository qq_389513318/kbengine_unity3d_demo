using System;

public class ItemStorage
{
    public const int ItemBagSlotCount = 256;
    public const int ItemBankSlotCount = 256;

    private Item[] m_equipItems = new Item[(int)ItemEquipSlot.ItemEquipSlotCount];
    private Item[] m_bagItems = new Item[ItemBagSlotCount];
    private Item[] m_bankItems = new Item[ItemBankSlotCount];
    private uint m_bagCapacity;
    private uint m_bankCapacity;

    public void LoadFromCreatePacket(NetPacket pck)
    {
        pck.Read(out m_bagCapacity);
        pck.Read(out m_bankCapacity);
        UnpackItemCluster(m_equipItems, pck);
        UnpackItemCluster(m_bagItems, pck);
        UnpackItemCluster(m_bankItems, pck);
    }

    private void UnpackItemCluster(Item[] pItems, NetPacket pck)
    {
        pck.Read(out UInt16 n);
        for (UInt16 i = 0; i < n; ++i)
        {
            pck.Read(out UInt32 slot);
            var pItem = new Item();
            pItem.LoadFromCreatePacket(slot, pck);
            pItems[slot] = pItem;
        }
    }
}
