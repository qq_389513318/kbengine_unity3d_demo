public class CreatureController : UnitController
{
    protected uint m_spawnId;

    public override string GetDisplayName()
    {
        return string.Format("{0}({1})", DBMgr.Instance.GetText(
            m_pProto.charTypeId, STRING_TEXT_TYPE.CHAR_NAME),
            GetS32Value((int)UNIT_PROPERTIES.UNIT_FIELD_LEVEL));
    }

    public override string GetName()
    {
        return DBMgr.Instance.GetText(
            m_pProto.charTypeId, STRING_TEXT_TYPE.CHAR_NAME);
    }

    public override void LoadFromCreatePacket(NetPacket pck)
    {
        base.LoadFromCreatePacket(pck);
        pck.Read(out m_spawnId);
    }
}