public class PlayerController :UnitController
{
    protected string m_name;

    public override string GetDisplayName()
    {
        return string.Format("{0}({1})", m_name,
            GetS32Value((int)UNIT_PROPERTIES.UNIT_FIELD_LEVEL));
    }

    public override string GetName()
    {
        return m_name;
    }

    public override void LoadFromCreatePacket(NetPacket pck)
    {
        base.LoadFromCreatePacket(pck);
        pck.Read(out m_name);
    }
}
