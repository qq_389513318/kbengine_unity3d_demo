using UnityEngine;

public class StaticObjectController : LocatableObjectController
{
    protected SObjPrototype m_pProto;
    protected uint m_spawnId;
    protected float m_radius;

    public string GetDisplayName()
    {
        return string.Format("{0}", DBMgr.Instance.GetText(
            m_pProto.sobjTypeId, STRING_TEXT_TYPE.SOBJ_NAME));
    }

    void OnGUI()
    {
        var worldPos = new Vector3(transform.position.x,
            transform.position.y + 1.0f, transform.position.z);
        var screenPos = Camera.main.WorldToScreenPoint(worldPos);
        var uiPos = new Vector2(screenPos.x, Screen.height - screenPos.y);

        var name = GetDisplayName();
        var nameSize = GUI.skin.label.CalcSize(new GUIContent(name));
        GUI.color = Color.yellow;
        GUI.Label(new Rect(uiPos.x - nameSize.x / 2,
            uiPos.y - nameSize.y - 1.0f, nameSize.x, nameSize.y), name);
    }

    public override void LoadFromCreatePacket(NetPacket pck)
    {
        base.LoadFromCreatePacket(pck);
        pck.Read(out uint sobjTypeId);
        pck.Read(out m_spawnId);
        pck.Read(out m_radius);

        m_pProto = DBMgr.Instance.tblSObjPrototype.GetEntry(sobjTypeId);
        Debug.Assert(m_pProto != null);
    }
}
