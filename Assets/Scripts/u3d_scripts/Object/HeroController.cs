using UnityEngine;

public class HeroController : PlayerController
{
    public ItemStorage ItemStorage { get; } = new ItemStorage();
    public QuestStorage QuestStorage { get; } = new QuestStorage();

    private Vector3 m_lastPos;
    private Quaternion m_lastDir;

    public ObjGUID TargetGuid { get; set; }

    void FixedUpdate()
    {
        if (transform.position != m_lastPos || transform.rotation != m_lastDir)
        {
            var pack = new NetPacket((int)GAME_OPCODE.CMSG_MOVE_SYNC);
            var pckEx = new NetPacketHelper(pack);
            m_lastPos = transform.position;
            m_lastDir = transform.rotation;
            pckEx.Write(m_lastPos);
            pckEx.Write(transform.forward);
            GameSession.Instance.PushSendPacket(pack);
        }
    }

    void Update()
    {
    }

    public override void LoadFromCreatePacket(NetPacket pck)
    {
        base.LoadFromCreatePacket(pck);
        ItemStorage.LoadFromCreatePacket(pck);
        QuestStorage.LoadFromCreatePacket(pck);

        m_lastPos = transform.position;
        m_lastDir = transform.rotation;
    }
}
