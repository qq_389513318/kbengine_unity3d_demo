using UnityEngine;

public abstract class LocatableObjectController : ObjectController
{
    protected InstGUID m_instGuid;

    public override void LoadFromCreatePacket(NetPacket pck)
    {
        base.LoadFromCreatePacket(pck);

        var pckEx = new NetPacketHelper(pck);
        pckEx.Read(out m_instGuid);
        pckEx.Read(out Vector3 pos);
        pckEx.Read(out Vector3 dir);

        transform.position = pos;
        transform.LookAt(dir);
    }

    public Vector3 Position
    {
        get
        {
            return transform.position;
        }
        set
        {
            transform.position = value;
        }
    }

    public Vector3 Direction
    {
        get
        {
            return transform.forward;
        }
        set
        {
            transform.LookAt(value);
        }
    }
}
