﻿using UnityEngine;

public class ShowFPS : MonoBehaviour
{
    public readonly float m_fUpdateInterval = 1.0F;

    private float m_fLastInterval;
    private int m_iFrames;
    private float m_fFps;

    void Start()
    {
        m_fLastInterval = Time.realtimeSinceStartup;
    }

    void OnGUI()
    {
        GUI.Label(new Rect(Screen.width - 40, 1, 200, 200), m_fFps.ToString("f2"));
    }

    void Update()
    {
        m_iFrames += 1;
        if (Time.realtimeSinceStartup > m_fLastInterval + m_fUpdateInterval)
        {
            m_fFps = m_iFrames / (Time.realtimeSinceStartup - m_fLastInterval);
            m_fLastInterval = Time.realtimeSinceStartup;
            m_iFrames = 0;
        }
    }
}
