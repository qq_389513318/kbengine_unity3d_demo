using System.Collections.Generic;

public class inst_string_text_list : BinaryHelper.ISerializable
{
	public uint stringID;
	public string stringEN;
	public string stringCN;

	public static string GetTableName() {
		return "inst_string_text_list";
	}
	public static string GetTableKeyName() {
		return "stringID";
	}
	public uint GetTableKeyValue() {
		return (uint)stringID;
	}
	public virtual void LoadFromBuffer(NetBuffer buffer) {
		BinaryHelper.ReadFromBuffer(out stringID, buffer);
		BinaryHelper.ReadFromBuffer(out stringEN, buffer);
		BinaryHelper.ReadFromBuffer(out stringCN, buffer);
	}
	public virtual void SaveToBuffer(NetBuffer buffer) {
		BinaryHelper.WriteToBuffer(stringID, buffer);
		BinaryHelper.WriteToBuffer(stringEN, buffer);
		BinaryHelper.WriteToBuffer(stringCN, buffer);
	}
};
